import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection
} from "angularfire2/firestore";
import { map } from "rxjs/operators";

import { Observable } from "rxjs";
import { DinamicSliderComponent } from "../components/dinamic-slider/dinamic-slider.component";
import { ProfileOneComponent } from "../components/profile/profile-dinamic.component";
import { ProcessItem } from "../components/ProcessItem";

export interface Todo {
  id?: string;
  circle: boolean;
  normal: boolean;
}

@Injectable({
  providedIn: "root"
})
export class FirebaseComponentConfig {
  private feedCollection: AngularFirestoreCollection<any>;
  private profileCollection: AngularFirestoreCollection<any>;

  private feed: Observable<any>;
  private profile: Observable<any>;

  constructor(db: AngularFirestore) {
    this.feedCollection = db.collection<any>(
      "/prueba/JxCiJk8RX7j8yhyTW63o/feed"
    );
    this.profileCollection = db.collection<any>(
      "/prueba/JxCiJk8RX7j8yhyTW63o/side-menu"
    );

    this.feed = this.feedCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );

    this.profile = this.profileCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getTodos() {
    return this.feed;
  }

  getPorfile() {
    return this.profile;
  }

  getProcessSteps(items): ProcessItem[] {
    return this.getPageOrder(items);
  }

  private getPageOrder(config): ProcessItem[] {
    let result: ProcessItem[] = [];

    for (let item of config) {
      if (item.type === "slider") {
        if (item.flag) {
          let comp = this.resolveComponentsFeed();
          let newItem = new ProcessItem(comp, item);
          result.push(newItem);
        }
      }
      if (item.type === "profile") {
        let comp = this.resolveComponentsProfile();
        let newItem = new ProcessItem(comp, item);
        result.push(newItem);
      }
    }

    return result;
  }

  private resolveComponentsFeed() {
    return DinamicSliderComponent;
  }

  private resolveComponentsProfile() {
    return ProfileOneComponent;
  }
}
