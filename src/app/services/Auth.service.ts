import { Injectable } from "@angular/core";
import * as jwt from "jsonwebtoken";
import { Config } from "../config/Config";
import { HttpClient, HttpHeaders, HttpRequest } from "@angular/common/http";
import { Observable, from, of, interval, throwError } from "rxjs";
import { map, flatMap } from "rxjs/operators";

import { HelpersService } from "./Helpers.service";
import { StorageService } from "./Storage.service";
import { EncrypDecryptService } from "./EncrypDecrypt.service";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  appSecret: string = Config.APP_SECRET;
  timeToken: number = Config.TOKEN_TIME; // tiempo del token consultado
  payload: any = {
    iss: "CM3.0APP",
    sub: "AppToken",
    app_name: "CanalMobile",
    app_id: "686C157C-5613-4E7F-8943-45964002F888",
    aud: "CMAPI",
    nbf: "",
    iat: "",
    exp: ""
  };

  constructor(
    private helperService: HelpersService,
    private storageService: StorageService,
    private httpClient: HttpClient,
    private encrypDecrypt: EncrypDecryptService
  ) {}

  // crear token inciial con estructura JWT
  localTokenJWT(): Observable<any> {
    return this.helperService.serverTime$().pipe(
      map((time: any) => {
        const date = new Date(time.utc).getTime() / 1000;
        this.payload.nbf = date - 1;
        this.payload.iat = date;
        this.payload.exp = date + 120;
        let JWT = jwt.sign(this.payload, this.appSecret);
        return JWT;
      })
    );
  }

  //acceso login
  login(token): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append("Authorization", `Bearer ${token}`);
    headers = headers.append("Content-Type", "application/json");
    headers = headers.append("Accept", "application/json");
    headers = headers.append("Accept-Language", "es-CL;q=0.1,en;q=0.3");
    headers = headers.append("Cache-Control", "no-cache");

    let dataUser = {
      user: "17671428-k",
      password: "holahola99",
      Uuid: "04567"
    };

    return this.httpClient.post(`${Config.URL}Account/login`, dataUser, {
      headers
    });
  }

  //agregar token en la cabezera
  addToken(request: HttpRequest<any>, token: any) {
    if (token) {
      let clone: HttpRequest<any>;
      clone = request.clone({
        setHeaders: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`
        }
      });
      return clone;
    }
    return request;
  }

  // refrescar token de acceso para los servicios con sesionó iniciada
  refresh$(): Observable<any> {
    return this.mergeToken$().pipe(
      flatMap(tokens => {
        const body = { refresh_token: tokens.token_refresh, uuid: "002" };
        return this.httpClient.post(
          `${Config.URL}Account/refreshAccessToken`,
          body,
          this.header(tokens.token_auth)
        );
      })
    );
  }

  //Crear objeto con TOKEN_REQUEST y TOKEN_REFRESH
  mergeToken$(): Observable<any> {
    return this.tokenAuthorize$().pipe(
      flatMap(tokenLocal => {
        return from(this.storageService.getTokenStorage("TOKEN_REFRESH")).pipe(
          map(tokenRefresh => {
            return {
              token_auth: tokenLocal,
              token_refresh: tokenRefresh.RefreshToken
            };
          })
        );
      })
    );
  }

  // SE GENERA TOKEN APP Y SE ENVIA PARA GENERAR TOKEN REQUEST
  tokenAuthorize$(): Observable<any> {
    return this.localTokenJWT().pipe(
      map(tokenLocal => tokenLocal),
      flatMap((tokenLocal: any) => {
        return this.httpClient
          .get(`${Config.URL}Authorize`, this.header(tokenLocal))
          .pipe(
            map((token: any) => {
              return token;
            })
          );
      })
    );
  }

  //contruye cabezera con token Bearer
  header(token?: any) {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append("Authorization", `Bearer ${token}`);
    headers = headers.append("Content-Type", "application/json");
    return { headers };
  }
}
