import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { Config } from "../config/Config";
import { CacheServiceApiRest } from "./CacheApi.service";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { OfflineManagerService } from "./OfflineManagerService";
import { NetworkService, ConnectionStatus } from "./Network.service";
import { Apollo } from "apollo-angular";
import gql from "graphql-tag";

const Feed = gql`
  query Feed($start: Int!) {
    feed(start: $start, take: 20) {
      name
      image
      type
    }
  }
`;

@Injectable({
  providedIn: "root"
})
export class HomeService {
  private status: BehaviorSubject<any> = new BehaviorSubject([]);

  constructor(
    private http: HttpClient,
    private cache: CacheServiceApiRest,
    private offline: OfflineManagerService,
    private newteork: NetworkService,
    private apollo: Apollo
  ) {
    this.content();
  }

  paginationFeed(
    group: string,
    type: string,
    pagination: number,
    publicDate?: number
  ) {
    const key: string = "/assets/data/feed.json";
    return this.cache.cacheGetApi(type, key, group);
  }

  async content(): Promise<any> {
    const pagination = 0;
    const publicDate = 0;
    const key: string = "/assets/data/feed.json";
    let data = await this.cache.cacheGetApi("get", key, "feed");
    this.status.next(data);
  }

  public sendPost(data) {
    if (this.newteork.getCurrentNetworkStatus() == ConnectionStatus.Offline) {
      return this.offline.storeRequest(
        `${Config.URL}"/enviar/feed"`,
        "post",
        data
      );
    } else {
      return this.http.post(`${Config.URL}"/enviar/feed"`, data).pipe(
        catchError(error => {
          this.offline.storeRequest(
            `${Config.URL}"/enviar/megusta"`,
            "post",
            data
          );
          throw new Error(error);
        })
      );
    }
  }

  public onfeed$(start: number): Observable<any> {
    return this.apollo.watchQuery<any>({
      query: Feed,
      variables: {
        start: start
      }
    }).valueChanges;
  }

  public onFeedChange$(): Observable<any> {
    return this.status.asObservable();
  }

  public onFeedStatus$(): Observable<any> {
    return this.status.getValue();
  }
}
