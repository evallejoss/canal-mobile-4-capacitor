import { Injectable } from "@angular/core";
import { AngularFirestore } from "angularfire2/firestore";

import { User, ApiChange, configGlobal } from "../interfaces/interfaceUser";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class FirebaseAPPConfig {
  dataUser: User;
  configApp: configGlobal;

  constructor(private db: AngularFirestore) {}

  async addUser(data: User, api: ApiChange): Promise<any> {
    try {
      await this.db
        .collection("/users")
        .doc(data.uuid)
        .set({
          data
        });
      await this.db
        .collection("/apichange")
        .doc(data.uuid)
        .set({
          api
        });
      return Promise.resolve();
    } catch (error) {
      throw error;
    }
  }

  firebaseGlobalConfig$(): Observable<configGlobal> {
    return this.db
      .collection("config")
      .doc("app")
      .snapshotChanges()
      .pipe(
        map(doc => {
          return doc.payload.data() as configGlobal;
        })
      );
  }

  templateStyle() {
    this.firebaseGlobalConfig$().subscribe((resp: configGlobal) => {
      const theme = document.querySelector("body");
      theme.style.setProperty("--colorAvatar", resp.colorBackPrimary);
      theme.style.setProperty("--colorTab", resp.colorBackPrimary);
      theme.style.setProperty("--varImgeCompany", resp.headerImage);
    });
  }
}
