import { Injectable } from "@angular/core";
import { Apollo } from "apollo-angular";
import gql from "graphql-tag";

const menu = gql`
  {
    menu {
      sideMenu {
        header {
          id
          lastname
          picture
          additionalData
        }
        items {
          name
          icon
          action {
            __typename
            ... on NavigateAction {
              route
              routerDirection
              params
            }
          }
        }
      }
      footerMenu {
        name
        icon
      }
    }
  }
`;

@Injectable({
  providedIn: "root"
})
export class MenuService {
  constructor(private apollo: Apollo) {}

  public build() {
    return this.apollo.watchQuery<any>({
      query: menu
    }).valueChanges;
  }
}
