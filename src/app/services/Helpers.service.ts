import { Injectable } from "@angular/core";
import { HttpClient, HttpBackend } from "@angular/common/http";
import { Config } from "../config/Config";

@Injectable({
  providedIn: "root"
})
export class HelpersService {
  private http: HttpClient;

  constructor(hadler: HttpBackend) {
    this.http = new HttpClient(hadler); // esto evita que los servicios llamer por los interceptores
  }

  serverTime$() {
    return this.http.get(`${Config.URL}Server/Time`);
  }
}
