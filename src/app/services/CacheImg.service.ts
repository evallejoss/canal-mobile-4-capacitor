import { Injectable, OnInit } from "@angular/core";
// import * as moment from "moment";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Config } from "../config/Config";
import db from "../config/cacheDB";

@Injectable({
  providedIn: "root"
})
export class CacheImgService implements OnInit {
  headers: HttpHeaders; // headers por defecto
  qualite: any; // calidad de imagen
  widht: number; // ancho de imagen
  height: any; // alto de imagen
  db: any;

  constructor(private http: HttpClient) {}

  ngOnInit() {}

  async getImg(src): Promise<any> {
    let blob: any = await db
      .table("images")
      .where("url")
      .equals(src)
      .limit(1)
      .toArray();
    if (blob.length > 0) {
      return await this.readDataBlob(blob[0].image);
    }

    let _imgBlob = await this.http
      .get(`${Config.URL}images?source=${src}&q=${""}&w=${300}&h=${300}`, {
        responseType: "blob",
        headers: this.headers
      })
      .toPromise();
    await db.table("images").put({
      url: src,
      image: _imgBlob
    });
    return await this.readDataBlob(_imgBlob);
  }

  readDataBlob(_imgBlob): Promise<any> {
    return new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.readAsDataURL(_imgBlob);
      reader.onloadend = () => {
        resolve(reader.result);
      };
    });
  }
}
