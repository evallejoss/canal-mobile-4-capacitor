import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

import { Plugins } from "@capacitor/core";
const { Toast } = Plugins;
const { Network } = Plugins;

export enum ConnectionStatus {
  Online,
  Offline
}

@Injectable({
  providedIn: "root"
})
export class NetworkService {
  private status: BehaviorSubject<ConnectionStatus> = new BehaviorSubject(
    ConnectionStatus.Offline
  );

  constructor() {
    this.initializeNetwork();
    this.initializeNetworkEvents();
  }

  private async initializeNetworkEvents() {
    let _status = await Network.getStatus();
    let _statusNetwork =
      _status.connectionType !== "none"
        ? ConnectionStatus.Online
        : ConnectionStatus.Offline;
    this.status.next(_statusNetwork);
  }

  private initializeNetwork() {
    Network.addListener("networkStatusChange", status => {
      if (status.connected) {
        this.updateNetworkStatus(ConnectionStatus.Online);
      } else {
        this.updateNetworkStatus(ConnectionStatus.Offline);
      }
    });
  }

  private async updateNetworkStatus(status: ConnectionStatus) {
    this.status.next(status);

    // let connection = status == ConnectionStatus.Offline ? "Offline" : "Online";
    // // await Toast.show({
    // //   text: `Estas en modo ${connection}`
    // // });
  }

  public onNetworkChange(): Observable<ConnectionStatus> {
    return this.status.asObservable();
  }

  public getCurrentNetworkStatus(): ConnectionStatus {
    return this.status.getValue();
  }
}
