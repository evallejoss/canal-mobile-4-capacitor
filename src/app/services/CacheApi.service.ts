import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import db from "../config/cacheDB";
import { StorageRequest } from "../interfaces/interfaceStorageRequest";
import { PreloadAllModules } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class CacheServiceApiRest {
  constructor(private http: HttpClient) {}

  // verifica si esta en cache el request y si expiro la fecha.
  // Inmediatamente realiza la llamada al servicio y lo remplaza
  async cacheGetApi<T>(
    type: string,
    key: string,
    group?: string
  ): Promise<any> {
    let object: any = await db
      .table("apis")
      .where("url")
      .equals(key)
      .limit(1)
      .toArray();
    if (object.length > 0) {
      return Promise.resolve(object[0].data);
    } else {
      let response = await this.http
        .get<T>(key, { observe: "response" })
        .toPromise();
      await db.table("apis").put({
        url: key,
        type: type,
        data: response.body,
        group: group !== "" ? group : "",
        time: new Date().getTime(),
        id: Math.random()
          .toString(36)
          .replace(/[^a-z]+/g, "")
          .substr(0, 5)
      });
      return Promise.resolve(response.body);
    }
  }

  async storeCachePost(url: string, type: string, data: any): Promise<any> {
    let action: StorageRequest = {
      url: url,
      type: type,
      data: data,
      group: "",
      time: new Date().getTime(),
      id: Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
        .substr(0, 5)
    };
    console.log(action);
    debugger;
    return await db.table("offline").put(action);
  }

  async getStorageCachePost(): Promise<Array<any>> {
    let filter = [];
    let post = await db.table("offline").toArray();
    filter = post.filter(object => {
      return object.type == "post";
    });
    return Promise.resolve(filter);
  }

  async getParamsStorageCachePost(url: string, type: string) {
    return await db
      .table("offline")
      .where("url")
      .equals(url)
      .and(resp => resp.type == type)
      .toArray();
  }

  async clearCacheApiKey(key: string, type: string): Promise<any> {
    try {
      if (key && type) {
        const deleteActionApi = await db
          .table("apis")
          .where("url")
          .equals(key)
          .and(resp => resp.type == type)
          .delete();
        return Promise.resolve(deleteActionApi);
      }
      return Promise.resolve("Falta pasar parametros");
    } catch (error) {
      throw "No se pudo eliminar el cache Rest Api" + error;
    }
  }

  async clearCacheApiGroup(type: string, group: string): Promise<any> {
    try {
      if (type && group) {
        const deleteActionApi = await db
          .table("apis")
          .where("url")
          .equals(type)
          .and(resp => resp.group == group)
          .delete();
        return Promise.resolve(deleteActionApi);
      }
      return Promise.resolve("Falta pasar parametros");
    } catch (error) {
      throw "No se elimino cache Rest Api" + error;
    }
  }

  async clearCacheApiType(type: string): Promise<any> {
    try {
      if (type) {
        const deleteActionApi = await db.table("offline").clear();
        return Promise.resolve(deleteActionApi);
      }
      return Promise.resolve("Falta pasar parametros");
    } catch (error) {
      throw "No se pudo eliminar el cache Rest Api" + error;
    }
  }
}
