import { Injectable } from "@angular/core";
import { AngularFirestore } from "angularfire2/firestore";

@Injectable({
  providedIn: "root"
})
export class FirebaseSocket {
  constructor(private db: AngularFirestore) {}

  socketFeed$(uuid: string) {
    return this.db
      .collection("apichange")
      .doc(uuid)
      .snapshotChanges();
  }

  socketFeedUpdate$(uuid: string) {
    this.db
      .collection("apichange")
      .doc(uuid)
      .update({
        api: {
          feed: {
            quantity: 0,
            date: new Date().getDate()
          }
        }
      });
  }
}
