import { Injectable } from "@angular/core";

import db from "../config/cacheDB";
import { User } from "../interfaces/interfaceUser";

@Injectable({
  providedIn: "root"
})
export class CacheConfigUser {
  constructor() {}

  // verifica si esta en cache el request y si expiro la fecha.
  // Inmediatamente realiza la llamada al servicio y lo remplaza
  async setCacheConfig<T>(dataUser: User): Promise<any> {
    let object: any = await db
      .table("config")
      .where("user")
      .equals(dataUser.uuid)
      .limit(1)
      .toArray();
    if (object.length == 0) {
      let resp = await db.table("config").put({
        user: dataUser.uuid,
        dataUser
      });
    }
    return Promise.resolve();
  }

  async getCacheConfig(): Promise<string> {
    const object = await db.table("config").toArray();
    if (object.length > 0) {
      return Promise.resolve(object[0].user);
    }
    return Promise.reject("no existe config de usuario");
  }
}
