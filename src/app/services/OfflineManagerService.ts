import { Injectable } from "@angular/core";
import { forkJoin, from, of, Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { switchMap, finalize } from "rxjs/operators";
import { StorageRequest } from "../interfaces/interfaceStorageRequest";
import { CacheServiceApiRest } from "./CacheApi.service";

@Injectable({
  providedIn: "root"
})
export class OfflineManagerService {
  constructor(
    private http: HttpClient,
    private cacheApi: CacheServiceApiRest
  ) {}

  checkForEvents(): Observable<any> {
    return from(this.cacheApi.getStorageCachePost()).pipe(
      switchMap(storeOperations => {
        if (storeOperations && storeOperations.length > 0) {
          return this.sendRequests(storeOperations).pipe(
            finalize(() => {
              //eliminar objeto de storage
            })
          );
        } else {
          return of(false);
        }
      })
    );
  }

  async storeRequest(url, type, data) {
    const dataStorage = await this.cacheApi.getParamsStorageCachePost(
      url,
      type
    );
    debugger;
    if (dataStorage.length > 0) {
      return;
    } else {
      return await this.cacheApi.storeCachePost(url, type, data);
    }
  }

  sendRequests(operations: StorageRequest[]) {
    let obs = [];
    for (let op of operations) {
      let oneObs = this.http.request(op.type, op.url, op.data);
      obs.push(oneObs);
    }

    return forkJoin(obs);
  }
}
