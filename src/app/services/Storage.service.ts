import { Injectable } from "@angular/core";
import { Plugins } from "@capacitor/core";
const { Storage } = Plugins;
const storage = window.localStorage;

@Injectable({
  providedIn: "root"
})
export class StorageService {
  constructor() {}

  // guarda objeto asociado a la llave
  setTokenStorage(key, data): Promise<void> {
    return new Promise((resolve, reject) => {
      try {
        storage && storage.setItem(key, JSON.stringify(data));
        resolve();
      } catch (err) {
        reject(`No existe el objeto en storage ${err}`);
      }
    });
  }

  // retorna el objeto guardado segun la llave
  getTokenStorage(key): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        if (storage) {
          const item = storage.getItem(key);
          resolve(JSON.parse(item));
        }
        resolve(undefined);
      } catch (err) {
        reject(`No se puede acceder al objeto: ${err}`);
      }
    });
  }

  // eleiminar objeto por llave
  async storageRemove(key) {
    return await Storage.remove({ key: key });
  }
}
