import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { ComponentsModule } from "./components/components.module";
import { HttpClientModule } from "@angular/common/http";
import { AngularFireModule } from "angularfire2";
import { AngularFireStorageModule } from "angularfire2/storage";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";

import { Config } from "./config/Config";
import { interceptorProviders } from "./config/interceptor";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { SliderNormalComponent } from "../app/components/slider-normal/slider-normal.component";
import { SliderCircleComponent } from "../app/components/slider-circle/slider-circle.component";
import { DinamicSliderComponent } from "../app/components/dinamic-slider/dinamic-slider.component";
import { ProfileOneComponent } from "./components/profile/profile-dinamic.component";
import { GraphQLModule } from "./graphql.module";
library.add(fas, far, fab);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [
    SliderNormalComponent,
    SliderCircleComponent,
    DinamicSliderComponent,
    ProfileOneComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    ComponentsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(Config.FIREBASE),
    AngularFirestoreModule.enablePersistence(),
    AngularFirestoreModule,
    AngularFireStorageModule,
    BrowserAnimationsModule,
    GraphQLModule,
    FontAwesomeModule
  ],
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy
    },
    interceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
