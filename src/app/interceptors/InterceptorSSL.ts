import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpEvent,
  HttpRequest,
  HttpHandler
} from "@angular/common/http";
import { Observable, from, of } from "rxjs";
import { catchError, mergeMap } from "rxjs/operators";
import { Platform } from "@ionic/angular";
import { Config } from "../config/Config";
import { AuthService } from "../services/Auth.service";

declare var window: any;

@Injectable({
  providedIn: "root"
})
export class InterceptorSSL implements HttpInterceptor {
  constructor(private auth: AuthService, private platform: Platform) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!this.platform.is("cordova")) {
      return next.handle(request);
    }

    return this.checkSecurity(Config.URL, request.url).pipe(
      mergeMap(modifiedReq => {
        let newReq = null;
        if (modifiedReq["message"] === "CONNECTION_SECURE") {
          newReq = request.clone({
            url: request.url
          });
        }
        return next.handle(newReq);
      })
    );
  }

  checkSecurity(URL, request) {
    return new Observable(observer => {
      (<any>window).plugins.sslCertificateChecker.check(
        message => {
          return observer.next({ req: request, message: message });
        },
        message => {
          return observer.error({ req: request, message: message });
        },
        URL,
        Config.FINGER_PRINT
      );
    });
  }
}
