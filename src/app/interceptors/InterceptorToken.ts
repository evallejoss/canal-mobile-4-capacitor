import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpResponse
} from "@angular/common/http";
import { Observable, from, throwError, of } from "rxjs";
import {
  catchError,
  mergeMap,
  map,
  tap,
  flatMap,
  concatMap
} from "rxjs/operators";
import { AuthService } from "../services/Auth.service";
import { StorageService } from "../services/Storage.service";
import { EncrypDecryptService } from "../services/EncrypDecrypt.service";

@Injectable({
  providedIn: "root"
})
export class InterceptorTokenService implements HttpInterceptor {
  constructor(
    private auth: AuthService,
    private storage: StorageService,
    private encrypDecrypt: EncrypDecryptService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const rest = [
      "Account/login",
      "Authorize",
      "Server/Time",
      "assets/data/",
      "Account/refreshAccessToken",
      "images?source"
    ];
    const filter = rest.filter(url => {
      return request.url.indexOf(url) !== -1;
    });

    if (filter.length === 0) {
      return from(this.storage.getTokenStorage("TOKEN_REFRESH")).pipe(
        flatMap(token => {
          request = this.auth.addToken(request, token.AccessToken);
          return next.handle(request).pipe(
            map(event => {
              if (event instanceof HttpResponse) {
                event = event.clone({ body: event.body });
              }
              return event;
            })
          );
        })
      );
    } else {
      return next.handle(request);
    }
  }
}
