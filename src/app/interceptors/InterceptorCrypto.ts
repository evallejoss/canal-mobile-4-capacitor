import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { EncrypDecryptService } from "../services/EncrypDecrypt.service";

@Injectable({
  providedIn: "root"
})
export class InterceptorCryptoService implements HttpInterceptor {
  constructor(private encrypDecrypt: EncrypDecryptService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (
      request.method !== "GET" &&
      request.url.indexOf("Account/login") == -1
    ) {
      //Encryptar datos del body
      let encrypt = this.encrypDecrypt.set(request.body);
      // Sobre escribo la llave body del request
      request = request.clone({ body: encrypt });
      return next.handle(request).pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            let decrypt = this.encrypDecrypt.get(event.body);
            event = event.clone({ body: decrypt });
          }
          return event;
        }),
        catchError((error: HttpErrorResponse) => {
          let data = {};
          data = {
            message: error.message,
            reason: error.status
          };
          return throwError(error);
        })
      );
    } else {
      return next.handle(request);
    }
  }
}
