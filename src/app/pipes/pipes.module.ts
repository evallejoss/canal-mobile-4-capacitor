import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterPipe } from './filter.pipe';
import { DomSanitizerPipe } from './dom-sanitizer.pipe';

@NgModule({
  declarations: [FilterPipe, DomSanitizerPipe],
  exports:[
    FilterPipe,
    DomSanitizerPipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
