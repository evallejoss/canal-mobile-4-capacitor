export interface SideMenu {
  icon: string;
  name: string;
  redirecTo: string;
}

export interface RootObject {
  Seccion: Seccion;
  Codigo: string;
  Tipo: string;
  Nombre: string;
  Resumen?: string;
  Detalle: string;
  Imagenes: (null | string | string)[];
  FechaPublicacion: number;
  Likes: any[];
  CantidadComentarios: number;
  CantidadTotalComentarios: number;
  CantidadLikes: number;
  Comentarios: Comentario[];
}

export interface Comentario {
  Usuario: Usuario;
  Comment: string;
  Fecha: number;
  CodigoComentario: string;
  ID: string;
}

export interface Usuario {
  ID: string;
  Nombre: string;
  Apellido: string;
  Foto: string;
  CodigoUsuario: string;
}

export interface Seccion {
  Codigo: string;
  Nombre: string;
  Padre: Padre;
}

export interface Padre {
  Codigo: string;
  Padre?: any;
}
