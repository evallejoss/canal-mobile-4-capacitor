export interface StorageRequest {
  url: string;
  type: string;
  time: number;
  data: any;
  id: string;
  group?: string;
}
