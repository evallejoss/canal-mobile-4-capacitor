export interface User {
  uuid: string;
  active: boolean;
  appVersion: string;
  osVersion: string;
  platform: string;
  model: string;
  isVirtual: boolean;
  notification: {
    quantity: number;
  };
}

export interface ApiChange {
  feed: {
    quantity: number;
    date: number;
  };
  notification: {
    quantity: number;
    date: number;
  };
}

export interface configGlobal {
  colorBackPrimary: string;
  colorBackSecondary: string;
  colorTerciario: string;
  colorBackHeader: string;
  colorBackFooter: string;
  colorTextPrimary: string;
  colorTextSecondary: string;
  colorTextTerciario: string;
  colorIconsFooter: string;
  headerImage: string;
  headerTitle: string;
}
