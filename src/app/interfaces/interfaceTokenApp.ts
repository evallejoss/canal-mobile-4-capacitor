

export interface TokenAPP{
    header : {alg:'HS256',typ:'JWT'},
    algorithm : "HS256",
    appSecret : "!QAZzaq12wsx"
    payload : {
        now : number,
        end : number,
        iss: "CM3.0APP",
        sub : "AppToken",
        nbf : number,
        iat: number,
        exp : number,
        app_name : 'CanalMobile',
        appId: string,
        aud : "CMAPI"
    }
}