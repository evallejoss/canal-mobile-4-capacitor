export interface SideMenu {
  header: {
    color: string;
    image_background: string;
    image: string;
    profile_name: string;
  };
  items: [
    {
      name: string;
      icon: string;
      notification: boolean;
      url: {
        url: string;
        internal: boolean;
        params: { items: [string] };
      };
      action: { name: string; params: { items: [string] } };
    }
  ];
}
