import { Component } from "@angular/core";
import { NavController } from "@ionic/angular";

import { StorageService } from "./services/Storage.service";
import { NetworkService, ConnectionStatus } from "./services/Network.service";
import { HomeService } from "./services/Home.service";
import { FirebaseAPPConfig } from "./services/FirebaseAPPConfig.service";
import { configGlobal } from "./interfaces/interfaceUser";
import { OfflineManagerService } from "./services/OfflineManagerService";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html"
})
export class AppComponent {
  constructor(
    private storageService: StorageService,
    private navCtrl: NavController,
    private network: NetworkService,
    private fireAppCfg: FirebaseAPPConfig,
    private offline: OfflineManagerService
  ) {
    this.validate();
    this.network.onNetworkChange().subscribe((status: ConnectionStatus) => {
      if (status == ConnectionStatus.Online) {
        //this.offline.checkForEvents().subscribe();
      }
    });
    this.fireAppCfg.templateStyle();
  }

  async validate() {
    let access = await this.storageService.getTokenStorage("LOGIN_ACCESS");
    if (access && access.login) {
      await this.navCtrl.navigateRoot("home");
    } else {
      this.navCtrl.navigateForward("login");
    }
  }

  templateStyle() {
    this.fireAppCfg;
  }
}
