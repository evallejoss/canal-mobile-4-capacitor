import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  { path: "home", redirectTo: "menu/main", data: { animation: "Home" } },
  {
    path: "menu",
    loadChildren: "./pages/menu/menu.module#MenuPageModule"
  },
  {
    path: "login",
    loadChildren: "./pages/login/login.module#LoginPageModule"
  },
  {
    path: "detail-feed",
    loadChildren: "./pages/detail-feed/detail-feed.module#DetailFeedPageModule"
  },
  {
    path: "modal-images",
    loadChildren:
      "./pages/modal-images/modal-images.module#ModalImagesPageModule"
  },
  { path: "", redirectTo: "login", pathMatch: "full" },
  {
    path: "feed-create",
    loadChildren:
      "./pages/modals/feed-create/feed-create.module#ModalFeedCreatePageModule"
  },
  { path: "share", loadChildren: "./pages/share/share.module#SharePageModule" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
