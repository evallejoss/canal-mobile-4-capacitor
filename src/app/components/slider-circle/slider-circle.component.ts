import { Component, OnInit } from "@angular/core";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";

@Component({
  selector: "app-slider-circle",
  templateUrl: "./slider-circle.component.html",
  styleUrls: ["./slider-circle.component.scss"],
  animations: [
    trigger("fadeIn", [
      state("void", style({ opacity: 0 })),
      transition("void => *", [
        style({ opacity: 0 }),
        animate("900ms ease-out", style({ opacity: 1 }))
      ])
    ])
  ]
})
export class SliderCircleComponent implements OnInit {
  storys: Array<any> = [];
  slideOpts: any = {};
  visible: boolean = false;
  constructor() {}

  ngOnInit() {
    this.visible = true;
    this.storys = [
      {
        img:
          "https://pbs.twimg.com/profile_images/1095548081892864000/toe7KWxm.jpg"
      },
      { img: "https://randomuser.me/api/portraits/women/68.jpg" },
      {
        img:
          "https://pbs.twimg.com/profile_images/1030175707736014851/L5sMj2sT.jpg"
      },
      {
        img:
          "https://pbs.twimg.com/profile_images/1030175707736014851/L5sMj2sT.jpg"
      }
    ];
    this.slideOpts = {
      initialSlide: 1,
      slidesPerView: 4,
      spaceBetween: 4,
      pagination: {
        el: ""
      }
    };
  }
}
