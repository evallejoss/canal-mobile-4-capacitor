import {
  Component,
  OnInit,
  Input,
  Renderer2,
  ViewChild,
  ElementRef
} from "@angular/core";
import { FirebaseAPPConfig } from "../../services/FirebaseAPPConfig.service";
import { configGlobal } from "../../interfaces/interfaceUser";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  @ViewChild("imageAvatar", { read: ElementRef }) private image: ElementRef;
  @ViewChild("avatar", { read: ElementRef }) private avatar: ElementRef;
  _title: string;
  headerImage: string;

  @Input() set title(text: string) {
    this._title = text !== undefined && text !== null ? text : "";
  }

  constructor(
    private fireAppCfg: FirebaseAPPConfig,
    private render: Renderer2
  ) {}

  update(isLoaded: boolean) {
    if (isLoaded) {
      // this.render.removeStyle(this.avatar.nativeElement, "display");
      this.render.addClass(this.image.nativeElement, "animated");
      this.render.addClass(this.image.nativeElement, "fadeIn");
    }
  }

  ngOnInit() {
    this.fireAppCfg.firebaseGlobalConfig$().subscribe((resp: configGlobal) => {
      console.log("avatar", resp);
      this.headerImage = resp.headerImage;
    });
  }
}
