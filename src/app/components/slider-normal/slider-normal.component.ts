import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider-normal',
  templateUrl: './slider-normal.component.html',
  styleUrls: ['./slider-normal.component.scss'],
})
export class SliderNormalComponent implements OnInit {

  banners : Array<any> = [];
  constructor() { }

  ngOnInit() {
    this.banners = [
      {img : '../assets/img/banner1.jpg'},
      {img : '../assets/img/banner2.jpg'},
      {img : '../assets/img/banner3.jpg'}
    ]
  }

  slideOpts = {
    autoplay : true,
    slidesPerView : 1,
    speed: 900,
    effect: 'fade',
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
  }
 
}
