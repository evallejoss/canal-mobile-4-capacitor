import {
  OnInit,
  ViewChild,
  Component,
  Input,
  ElementRef,
  Renderer2
} from "@angular/core";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import { Platform, ModalController } from "@ionic/angular";

import { CacheImgService } from "../../services/CacheImg.service";
import { HelpersService } from "../../services/Helpers.service";
import { ModalImagesPage } from "../../pages/modal-images/ModalPage.page";

@Component({
  selector: "app-preload-img",
  templateUrl: "./preload-img.component.html",
  styleUrls: ["./preload-img.component.scss"],
  providers: [CacheImgService, HelpersService],
  animations: [
    trigger("fadein", [
      state("void", style({ opacity: 0 })),
      transition("void => *", [
        style({ opacity: 0 }),
        animate("600ms ease-out", style({ opacity: 1 }))
      ])
    ])
  ]
})
export class PreloadImgComponent implements OnInit {
  @ViewChild("image", { read: ElementRef }) private image: ElementRef;
  _src: any;
  img: any;
  contador: number = 0;

  constructor(
    private _renderer: Renderer2,
    private cacheImg: CacheImgService,
    private platform: Platform,
    private modalCtrl: ModalController,
    private el: ElementRef
  ) {}

  @Input() set src(val: string) {
    this._src = val !== undefined && val !== null ? val : "";
  }

  ngOnInit() {
    this._loaded();
  }

  update(isLoaded: boolean) {
    if (isLoaded) {
      this._renderer.addClass(this.image.nativeElement, "animated");
      this._renderer.addClass(this.image.nativeElement, "fadeIn");
    } else {
      isLoaded = isLoaded;
    }
  }

  _loaded() {
    this.cacheImg.widht = this.platform.width();
    this.cacheImg.height = 300;
    this.cacheImg.qualite = "";
    if (this._src) {
      this.cacheImg.getImg(this._src).then(urlImg => {
        this.img = urlImg;
      });
    }
  }

  async imageZoom(img: string) {
    const modal = await this.modalCtrl.create({
      component: ModalImagesPage,
      componentProps: {
        img: img
      }
    });

    await modal.present();
    const { data } = await modal.onDidDismiss();
  }
}
