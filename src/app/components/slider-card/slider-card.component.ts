import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider-card',
  templateUrl: './slider-card.component.html',
  styleUrls: ['./slider-card.component.scss'],
})
export class SliderCardComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

  slideOpts = {
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
  }
  
}
