import { Component, OnInit, Input } from "@angular/core";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";
import { interfaceProcessComponentDinamic } from "src/app/interfaces/interfaceProcessComponentDinamic";

@Component({
  selector: "app-dinamic-slider",
  templateUrl: "./dinamic-slider.component.html",
  styleUrls: ["./dinamic-slider.component.scss"],
  animations: [
    trigger("fadeIn", [
      state("void", style({ opacity: 0 })),
      transition("void => *", [
        style({ opacity: 0 }),
        animate("900ms ease-out", style({ opacity: 1 }))
      ])
    ])
  ]
})
export class DinamicSliderComponent
  implements interfaceProcessComponentDinamic {
  @Input() set data(val: any) {
    this.classSlider = val.config.class;
    this.title = val.config.title;
    this.sliderOptions(val.config.class);
    this.storys = [
      { img: "../assets/img/banner1.png" },
      { img: "../assets/img/banner2.png" },
      { img: "../assets/img/banner3.png" },
      { img: "../assets/img/banner3.png" }
    ];
  }
  classSlider: string;
  title: string;
  slideOpts: any = [];

  constructor() {}

  storys: Array<any> = [];

  sliderOptions(type) {
    if (type == "normal") {
      this.slideOpts = {
        autoplay: false,
        slidesPerView: 1,
        height: "100px",
        speed: 900,
        effect: "fade",
        pagination: {
          el: ".swiper-pagination",
          clickable: true
        }
      };
    } else {
      this.slideOpts = {
        initialSlide: 1,
        slidesPerView: 4,
        spaceBetween: 4,
        pagination: {
          el: ""
        }
      };
    }
  }
}
