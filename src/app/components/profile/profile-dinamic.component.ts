import { Component, Input, ViewChild, ElementRef } from "@angular/core";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";

import { interfaceProcessComponentDinamic } from "../../interfaces/interfaceProcessComponentDinamic";
import { FirebaseAPPConfig } from "../../services/FirebaseAPPConfig.service";
import { MenuService } from "../../services/Menu.service";

@Component({
  selector: "app-profile-dinamic",
  templateUrl: "./profile-dinamic.component.html",
  styleUrls: ["./profile-dinamic.component.scss"],
  animations: [
    trigger("fadein", [
      state("load", style({ opacity: 0 })),
      transition("void => *", [
        style({ opacity: 0 }),
        animate("900ms ease-out", style({ opacity: 1 }))
      ])
    ])
  ]
})
export class ProfileOneComponent implements interfaceProcessComponentDinamic {
  @ViewChild("profile", { read: ElementRef }) tref: ElementRef;
  @Input() set data(val: any) {
    this.class = val.config.class;
    this.menuSrv.build().subscribe(result => {
      if (val.config.class == "one") {
        console.log(val.config.class);
        this.srcImg = result.data.menu.sideMenu.header.picture;
      } else {
        this.srcImg = "../assets/img/bg-head-circle-stbk.png";
      }
    });
  }
  srcImg: string;
  class: string;
  avatar: string;

  constructor(
    private fireAppCfg: FirebaseAPPConfig,
    private menuSrv: MenuService
  ) {}
}
