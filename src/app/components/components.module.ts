import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { HeaderComponent } from "./header/header.component";
import { PreloadImgComponent } from "./preload-img/preload-img.component";
import { SliderCircleComponent } from "./slider-circle/slider-circle.component";
import { SliderNormalComponent } from "./slider-normal/slider-normal.component";
import { SliderCardComponent } from "./slider-card/slider-card.component";
import { PipesModule } from "../pipes/pipes.module";
import { DinamicSliderComponent } from "./dinamic-slider/dinamic-slider.component";
import { ProfileOneComponent } from "./profile/profile-dinamic.component";
import { IteraccionesComponent } from "./iteracciones/iteracciones.component";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [
    HeaderComponent,
    PreloadImgComponent,
    SliderCircleComponent,
    SliderNormalComponent,
    SliderCardComponent,
    DinamicSliderComponent,
    ProfileOneComponent,
    IteraccionesComponent
  ],
  imports: [CommonModule, IonicModule, PipesModule, FontAwesomeModule],
  exports: [
    HeaderComponent,
    PreloadImgComponent,
    SliderCircleComponent,
    SliderNormalComponent,
    SliderCardComponent,
    DinamicSliderComponent,
    ProfileOneComponent,
    IteraccionesComponent
  ]
})
export class ComponentsModule {}
