import { Router } from "@angular/router";
import {
  Component,
  OnInit,
  ElementRef,
  Renderer2,
  ViewChild
} from "@angular/core";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

@Component({
  selector: "app-iteracciones",
  templateUrl: "./iteracciones.component.html",
  styleUrls: ["./iteracciones.component.scss"]
})
export class IteraccionesComponent implements OnInit {
  @ViewChild("heart", { read: ElementRef }) private heart: ElementRef;
  @ViewChild("text", { read: ElementRef }) private text: ElementRef;
  like: number = 3;

  constructor(
    private el: ElementRef,
    private render: Renderer2,
    private router: Router
  ) {}

  ngOnInit() {}

  changeHeart() {
    if (this.heart.nativeElement.outerHTML.indexOf("is_animating") == -1) {
      this.render.addClass(this.heart.nativeElement, "is_animating");
      //this.render.addClass(this.text.nativeElement, "textColor");
      this.like = 4;
    } else {
      this.render.removeClass(this.heart.nativeElement, "is_animating");
      //this.render.removeClass(this.text.nativeElement, "textColor");
      this.like--;
    }
  }

  detailFeed() {
    this.router.navigateByUrl("/detail-feed");
  }

  share() {
    this.router.navigateByUrl("/share");
  }
}
