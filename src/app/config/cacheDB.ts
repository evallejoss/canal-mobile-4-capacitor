import Dexie from "dexie";

const db = new Dexie("colabraDBTEST");
db.version(1).stores({
  images: "url",
  apis: "url",
  config: "user",
  offline: "url"
});

export default db;
