import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { IonicModule } from "@ionic/angular";

import { ModalImagesPage } from "./ModalPage.page";

@NgModule({
  imports: [CommonModule, IonicModule],
  declarations: [ModalImagesPage]
})
export class ModalImagesPageModule {}
