import { Component, OnInit, ViewChild, ElementRef, Input } from "@angular/core";
import { ModalController } from "@ionic/angular";

@Component({
  selector: "app-modal-images",
  templateUrl: "./modal-images.page.html",
  styleUrls: ["./modal-images.page.scss"]
})
export class ModalImagesPage implements OnInit {
  @Input() set img(val: string) {
    console.log("img modal", val);
    this.urlImg = val !== undefined && val !== null ? val : "";
  }

  @ViewChild("slider", { read: ElementRef }) slider: ElementRef;
  urlImg: any;
  defaultImg: any = "../../assets/img/logo_login.svg";

  configSlide = {
    zoom: {
      maxRatio: 5
    }
  };
  constructor(private modalCtrl: ModalController) {}

  ngOnInit() {}

  cerrar() {
    this.modalCtrl.dismiss();
  }

  salirSinArgumentos() {
    this.modalCtrl.dismiss();
  }
}
