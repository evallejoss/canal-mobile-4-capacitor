import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver
} from "@angular/core";
import { Router, RouterEvent } from "@angular/router";
import { MenuController } from "@ionic/angular";
import { FirebaseComponentConfig } from "../../services/FirebaseComponentConfig.service";
import { ProfileOneComponent } from "../../components/profile/profile-dinamic.component";
import { interfaceProcessComponentDinamic } from "../../interfaces/interfaceProcessComponentDinamic";
import { MenuService } from "../../services/Menu.service";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.page.html",
  styleUrls: ["./menu.page.scss"]
})
export class MenuPage implements OnInit {
  @ViewChild("processContainer", { read: ViewContainerRef }) container;
  selectedPath = "";
  items: Array<any> = [];
  header: Array<any> = [];

  constructor(
    private router: Router,
    private menu: MenuController,
    private fireService: FirebaseComponentConfig,
    private resolver: ComponentFactoryResolver,
    private menuSrv: MenuService
  ) {
    let componentRef;
    this.fireService.getPorfile().subscribe(res => {
      let data = this.fireService.getProcessSteps(res);
      this.container.clear();
      for (let item of data) {
        const factory = this.resolver.resolveComponentFactory(
          ProfileOneComponent
        );
        componentRef = this.container.createComponent(factory);
        (<interfaceProcessComponentDinamic>componentRef.instance).data = item;
      }
    });

    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
  }

  ngOnInit() {
    this.menuSrv.build().subscribe(result => {
      this.header = result.data.menu.sideMenu.header;
      this.items = result.data.menu.sideMenu.items;
    });
  }

  redirectView(view) {
    this.router.navigateByUrl(`/menu/main/tabs/${view}`);
  }

  closeMenu(view) {
    this.menu
      .close("first")
      .then(_ => {
        this.router.navigateByUrl("/menu/main/tabs/perfil");
      })
      .catch(error => {
        console.log(error);
      });
  }
}
