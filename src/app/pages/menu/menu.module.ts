import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { MenuPage } from "./Menu.page";
import { ComponentsModule } from "../../components/components.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

const routes: Routes = [
  {
    path: "",
    component: MenuPage,
    children: [
      {
        path: "main",
        loadChildren: "../main/main.module#MainPageModule"
      },
      {
        path: "main/tabs/perfil",
        loadChildren: "../perfil/perfil.module#PerfilPageModule"
      },
      {
        path: "main/tabs/beneficios",
        loadChildren: "../beneficios/beneficios.module#BeneficiosPageModule"
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    FontAwesomeModule
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
