import { Component, OnInit } from "@angular/core";
import { LoadingController, NavController } from "@ionic/angular";
import { Plugins } from "@capacitor/core";
const { SplashScreen, Device } = Plugins;

import { StorageService } from "../../services/Storage.service";
import { AuthService } from "../../services/Auth.service";
import { FirebaseAPPConfig } from "../../services/FirebaseAPPConfig.service";
import { User, ApiChange } from "../../interfaces/interfaceUser";
import { CacheConfigUser } from "../../services/CacheConfigUser.service";
import { HomeService } from "../../services/Home.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  public loginForm: any;
  spinner: boolean = false;

  userData: User = {
    uuid: "",
    active: false,
    appVersion: "",
    osVersion: "",
    platform: "",
    model: "",
    isVirtual: false,
    notification: {
      quantity: 0
    }
  };

  api: ApiChange = {
    feed: {
      quantity: 0,
      date: 0
    },
    notification: {
      quantity: 0,
      date: 0
    }
  };

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    private storageSrv: StorageService,
    private auth: AuthService,
    private initConfig: FirebaseAPPConfig,
    private cacheConfig: CacheConfigUser
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    SplashScreen.hide();
  }

  async login() {
    try {
      this.spinner = true;
      // let tokenRequest = await this.auth.tokenAuthorize$().toPromise();
      // let dataAuth = await this.auth.login(tokenRequest).toPromise();
      // let dataToken = {
      //   AccessToken: dataAuth.AccessToken,
      //   RefreshToken: dataAuth.RefreshToken
      // };
      // await this.storageSrv.setTokenStorage("TOKEN_REFRESH", dataToken);
      await this.storageSrv.setTokenStorage("LOGIN_ACCESS", { login: true });
      const info = await Device.getInfo();
      // Config inicial con parametros de usuario
      this.userData.uuid = info.uuid;
      this.userData.active = true;
      this.userData.appVersion = info.appVersion;
      this.userData.osVersion = info.osVersion;
      this.userData.platform = info.platform;
      this.userData.model = info.model;
      this.userData.isVirtual = info.isVirtual;
      this.userData.notification.quantity = 0;
      await this.initConfig.addUser(this.userData, this.api);
      await this.cacheConfig.setCacheConfig(this.userData);
      await this.navCtrl.navigateRoot("home");
    } catch (error) {
      this.spinner = false;
      throw error;
    }
  }

  ionViewWillLeave() {
    this.spinner = false;
  }
}
