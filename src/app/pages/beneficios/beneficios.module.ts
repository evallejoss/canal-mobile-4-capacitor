import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { BeneficiosPage } from "./Beneficios.page";
import { PipesModule } from "../../pipes/pipes.module";

const routes: Routes = [
  {
    path: "",
    component: BeneficiosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild(routes),
    PipesModule
  ],
  declarations: [BeneficiosPage]
})
export class BeneficiosPageModule {}
