import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-beneficios",
  templateUrl: "./beneficios.page.html",
  styleUrls: ["./beneficios.page.scss"]
})
export class BeneficiosPage implements OnInit {
  test: any[] = [
    { title: "test1" },
    { title: "test2" },
    { title: "test3" },
    { title: "test4" }
  ];

  textoBuscar = "";

  constructor() {}

  ngOnInit() {}

  buscar(event) {
    this.textoBuscar = event.detail.value;
  }
}
