import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { MainPage } from "./Main.page";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

const routes: Routes = [
  {
    path: "tabs",
    component: MainPage,
    children: [
      {
        path: "home",
        loadChildren: "../home/home.module#HomePageModule"
      },
      {
        path: "beneficios",
        loadChildren: "../beneficios/beneficios.module#BeneficiosPageModule"
      },
      {
        path: "perfil",
        loadChildren: "../perfil/perfil.module#PerfilPageModule"
      }
    ]
  },
  {
    path: "",
    redirectTo: "tabs/home",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild(routes),
    FontAwesomeModule
  ],
  declarations: [MainPage]
})
export class MainPageModule {}
