import { Component, OnInit, ViewChild } from "@angular/core";
import { MenuController, IonTabs } from "@ionic/angular";
import { Plugins } from "@capacitor/core";
const { LocalNotifications } = Plugins;

import { FirebaseSocket } from "../../services/FirebaseSocket";
import { CacheConfigUser } from "../../services/CacheConfigUser.service";
import { CacheServiceApiRest } from "../../services/CacheApi.service";
import { HomeService } from "../../services/Home.service";
import { Subject } from "rxjs";
import { MenuService } from "../../services/Menu.service";

@Component({
  selector: "app-main",
  templateUrl: "./main.page.html",
  styleUrls: ["./main.page.scss"]
})
export class MainPage implements OnInit {
  @ViewChild("myTabs") tabRef: IonTabs;
  notification: number = 0;
  newNotification = new Subject<number>();
  menu: Array<any> = [];

  constructor(
    private menuCtrl: MenuController,
    private backTask: FirebaseSocket,
    private configUser: CacheConfigUser,
    private cacheApi: CacheServiceApiRest,
    private srvHome: HomeService,
    private socketFrs: FirebaseSocket,
    private menuSrv: MenuService
  ) {}

  ngOnInit() {
    this.changeFeed();
    LocalNotifications.addListener(
      "localNotificationActionPerformed",
      action => {}
    );
    this.menuSrv.build().subscribe(result => {
      this.menu = result.data.menu.footerMenu.slice(1);
      console.log(this.menu);
    });
  }

  async changeFeed() {
    const uuid: string = await this.configUser.getCacheConfig();
    this.backTask.socketFeed$(uuid).subscribe((resp: any) => {
      this.updateFeed(resp.payload.data().api.feed.quantity);
    });
  }

  toggleMenu() {
    this.menuCtrl.open("end");
    this.menuCtrl.toggle();
  }

  async updateFeed(notification: number) {
    this.notificationUpdate(notification);
  }

  async notificationUpdate(notification: number) {
    if (notification > 0) {
      LocalNotifications.schedule({
        notifications: [
          {
            title: `Hay ${notification} contenidos nuevos`,
            body: "En el muro",
            id: 2,
            schedule: { at: new Date(Date.now() + 1000 * 1) },
            sound: "assets/sound/1111.aiff",
            attachments: null,
            actionTypeId: "",
            extra: null
          }
        ]
      });
      this.notification = notification;
    }
  }

  async onPressTab() {
    if (this.notification > 0) {
      await this.cacheApi.clearCacheApiGroup("get", "feed");
      this.srvHome.content();
      const uuid: string = await this.configUser.getCacheConfig();
      this.socketFrs.socketFeedUpdate$(uuid);
      this.notification = 0;
      this.newNotification.next(this.notification);
    }
  }
}
