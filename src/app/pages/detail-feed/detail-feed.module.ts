import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { DetailFeedPage } from "./detail-feed.page";
import { ParallaxHeaderDirective } from "../../directives/parallax-header.directive";

const routes: Routes = [
  {
    path: "",
    component: DetailFeedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetailFeedPage, ParallaxHeaderDirective]
})
export class DetailFeedPageModule {}
