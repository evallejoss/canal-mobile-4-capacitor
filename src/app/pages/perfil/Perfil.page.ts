import { Component, OnInit } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { MenuController } from "@ionic/angular";
import {
  trigger,
  state,
  style,
  animate,
  transition
} from "@angular/animations";

@Component({
  selector: "app-perfil",
  templateUrl: "./perfil.page.html",
  styleUrls: ["./perfil.page.scss"],
  animations: [
    trigger("fadein", [
      state("load", style({ opacity: 0 })),
      transition("void => *", [
        style({ opacity: 0 }),
        animate("900ms ease-out", style({ opacity: 1 }))
      ])
    ])
  ]
})
export class PerfilPage implements OnInit {
  imgHeaderCard = "../assets/img/Bg-login-alsea.svg";
  imgAvatar = "../assets/img/profile1.jpg";

  profile: Array<any> = [
    { icon: "send", name: "josefa@starbucks.cl", profile: true },
    { icon: "globe", name: "Providencia", profile: true },
    { icon: "phone-portrait", name: "(+569) 78934567", profile: true },
    {
      icon: "star-outline",
      name: "Division Starbucks Providencia",
      profile: true
    }
  ];

  messages = [
    {
      user: "Simon",
      createdAt: 1554090856000,
      msg: "Hola todo bien en el local?"
    },
    {
      user: "Josefa",
      createdAt: 1554090956000,
      msg: "excelente todos trabajando ya"
    },
    {
      user: "Simon",
      createdAt: 1554091056000,
      msg: "Muy Bien gracias!!!"
    }
  ];

  currentUser = "Simon";

  constructor(private sanitizer: DomSanitizer, private menu: MenuController) {}

  ngOnInit() {}
}
