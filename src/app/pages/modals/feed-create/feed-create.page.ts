import { Component, OnInit, ViewChild, ElementRef, Input } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { HomeService } from "../../../services/Home.service";

@Component({
  selector: "app-modal-images",
  templateUrl: "./feed-create.page.html",
  styleUrls: ["./feed-create.page.scss"]
})
export class ModalFeedCreatePage implements OnInit {
  // @Input() set img(val: string) {
  //   this.urlImg = val !== undefined && val !== null ? val : "";
  // }

  @ViewChild("slider", { read: ElementRef }) slider: ElementRef;

  constructor(
    private modalCtrl: ModalController,
    private homeSrv: HomeService
  ) {}

  ngOnInit() {}

  cerrar() {
    this.modalCtrl.dismiss();
  }

  salirSinArgumentos() {
    this.modalCtrl.dismiss();
  }

  sendPost() {
    const post = {
      mensaje: "testing",
      foto:
        "https://firebasestorage.googleapis.com/v0/b/colabra-b67c9.appspot.com/o/logo_app_canal_mobile.svg?alt=media&token=3036a149-0219-440f-af6e-74b3476bb906",
      texto: "ggwpxd"
    };
    this.homeSrv.sendPost(post);
  }
}
