import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { ModalFeedCreatePage } from "./feed-create.page";

const routes: Routes = [
  {
    path: "",
    component: ModalFeedCreatePage
  }
];

@NgModule({
  imports: [CommonModule, IonicModule],
  declarations: [ModalFeedCreatePage]
})
export class ModalFeedCreatePageModule {}
