import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-share",
  templateUrl: "./share.page.html",
  styleUrls: ["./share.page.scss"]
})
export class SharePage implements OnInit {
  test: any[] = [
    { title: "Conrado Trueba Dulce" },
    { title: "Celeste Brochero Frenes" },
    { title: "Gustavo Sorzano Cavada" },
    { title: "Klelia Cobian Echart" },
    { title: "Viannette Zapatero Parrondo" },
    { title: "Juliana Galarreta Fontiveros" },
    { title: "Anayda Bueras Sosa" },
    { title: "Iñaki Blanco Tovilleja" },
    { title: "Pilar Varez Rionda" },
    { title: "Ponciano Arena Rivacoba" }
  ];

  textoBuscar = "";

  constructor() {}

  ngOnInit() {}

  buscar(event) {
    this.textoBuscar = event.detail.value;
  }
}
