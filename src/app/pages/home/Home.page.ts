import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  ElementRef
} from "@angular/core";
import {
  IonInfiniteScroll,
  ModalController,
  Platform,
  IonContent
} from "@ionic/angular";
import * as moment from "moment";
import { ActionSheetController } from "@ionic/angular";
import { trigger, transition, animate, style } from "@angular/animations";
import { Plugins } from "@capacitor/core";
const { SplashScreen } = Plugins;

import { ModalFeedCreatePage } from "../modals/feed-create/feed-create.page";
import { FirebaseComponentConfig } from "../../services/FirebaseComponentConfig.service";
import { DinamicSliderComponent } from "../../components/dinamic-slider/dinamic-slider.component";
import { interfaceProcessComponentDinamic } from "../../interfaces/interfaceProcessComponentDinamic";
import { HomeService } from "../../services/Home.service";
import { RootObject } from "../../interfaces/interfaceContent";
import { MainPage } from "../main/Main.page";
import { FirebaseAPPConfig } from "../../services/FirebaseAPPConfig.service";
import { configGlobal } from "../../interfaces/interfaceUser";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
  animations: [
    trigger("fadein", [
      transition(":enter", [
        style({ transform: "translateY(-100%)" }),
        animate("200ms ease-in", style({ transform: "translateY(0%)" }))
      ]),
      transition(":leave", [
        animate("200ms ease-in", style({ transform: "translateY(-100%)" }))
      ])
    ])
  ]
})
export class HomePage implements OnInit {
  @ViewChild(IonInfiniteScroll) infinitiScroll: IonInfiniteScroll;
  @ViewChild("processContainer", { read: ViewContainerRef }) container;
  @ViewChild(IonContent) content: IonContent;
  @ViewChild("preloadImg") preloadImg: ElementRef;

  feeds: Array<RootObject> = [];
  loginUser: boolean = true;
  spinner: boolean = false;
  skeleton: boolean;
  title: string;
  date: any = moment().format("DD/MM/YYYY");
  pagination: number = 0;
  publicDate: number = 0;
  todo: any;

  constructor(
    private homeSrv: HomeService,
    private modalCtrl: ModalController,
    private fireService: FirebaseComponentConfig,
    private resolver: ComponentFactoryResolver,
    private main: MainPage,
    private fireConfig: FirebaseAPPConfig,
    public actionSheetController: ActionSheetController
  ) {}

  ngOnInit() {
    let componentRef;
    this.contentFeed("");
    this.fireService.getTodos().subscribe(res => {
      let data = this.fireService.getProcessSteps(res);
      this.container.clear();
      for (let item of data) {
        const factory = this.resolver.resolveComponentFactory(
          DinamicSliderComponent
        );
        componentRef = this.container.createComponent(factory);
        (<interfaceProcessComponentDinamic>componentRef.instance).data = item;
      }
    });

    this.main.newNotification.subscribe(_ => this.scrollToTop());
    this.fireConfig.firebaseGlobalConfig$().subscribe((resp: configGlobal) => {
      this.title = resp.headerTitle;
    });
  }

  ionViewDidEnter() {
    SplashScreen.hide();
  }

  doRefresh(event) {
    this.contentFeed(event);
  }

  contentFeed(event?: any) {
    this.feeds = [];
    this.infinitiScroll.disabled = false;
    this.homeSrv.onfeed$(this.pagination).subscribe(resp => {
      this.pagination += 20;
      this.feeds.push(...resp.data.feed);
      if (event) {
        event.target.complete();
      }
    });
  }

  loadData(event) {
    console.log("load feed");
    this.homeSrv.onfeed$(this.pagination).subscribe(resp => {
      this.pagination += 20;
      console.log(this.pagination);
      this.feeds.push(...resp.data.feed);
      console.log("feed", this.feeds.length);
      if (resp.data.feed.length == 0) {
        event.target.disabled = true;
      }
      event.target.complete();
    });
  }

  async feedCreate(img) {
    const modal = await this.modalCtrl.create({
      component: ModalFeedCreatePage,
      componentProps: {
        img: img
      }
    });

    await modal.present();
    const { data } = await modal.onDidDismiss();
  }

  scrollToTop() {
    this.content.scrollToTop(1500);
  }

  onClick() {}

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: "Acciones",
      buttons: [
        {
          text: "Eliminar",
          role: "destructive",
          icon: "trash",
          handler: () => {
            console.log("Delete clicked");
          }
        },
        {
          text: "Despublicar",
          icon: "arrow-dropright-circle",
          handler: () => {
            console.log("Share clicked");
          }
        }
      ]
    });
    await actionSheet.present();
  }
}
