import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { HomePage } from "./Home.page";
import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";
import { ModalImagesPage } from "../modal-images/ModalPage.page";
import { ModalImagesPageModule } from "../modal-images/modal-images.module";
import { ModalFeedCreatePage } from "../modals/feed-create/feed-create.page";
import { ModalFeedCreatePageModule } from "../modals/feed-create/feed-create.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

const routes: Routes = [
  {
    path: "",
    component: HomePage
  }
];

@NgModule({
  entryComponents: [ModalImagesPage, ModalFeedCreatePage],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    PipesModule,
    ModalImagesPageModule,
    ModalFeedCreatePageModule,
    FontAwesomeModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
