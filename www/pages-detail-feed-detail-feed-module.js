(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-detail-feed-detail-feed-module"],{

/***/ "./src/app/directives/parallax-header.directive.ts":
/*!*********************************************************!*\
  !*** ./src/app/directives/parallax-header.directive.ts ***!
  \*********************************************************/
/*! exports provided: ParallaxHeaderDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParallaxHeaderDirective", function() { return ParallaxHeaderDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var ParallaxHeaderDirective = /** @class */ (function () {
    function ParallaxHeaderDirective(element, renderer, domCtrl) {
        this.element = element;
        this.renderer = renderer;
        this.domCtrl = domCtrl;
    }
    ParallaxHeaderDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        console.log("height", this.headerHeight);
        this.headerHeight = this.parallaxHeight;
        this.mainContent = this.element.nativeElement.querySelector(".main-content");
        this.domCtrl.write(function () {
            _this.header = _this.renderer.createElement("div");
            _this.renderer.insertBefore(_this.element.nativeElement, _this.header, _this.element.nativeElement.firstChild);
            _this.renderer.setStyle(_this.header, "background-image", "url(" + _this.imagePath + ")");
            _this.renderer.setStyle(_this.header, "height", _this.headerHeight + "px");
            _this.renderer.setStyle(_this.header, "background-size", "cover");
        });
    };
    ParallaxHeaderDirective.prototype.onContentScroll = function (ev) {
        var _this = this;
        this.domCtrl.read(function () {
            var translateAmt, scaleAmt;
            if (ev.detail.scrollTop > _this.parallaxHeight) {
                return;
            }
            if (ev.detail.scrollTop >= 0) {
                translateAmt = -(ev.detail.scrollTop / 2);
                scaleAmt = 1;
            }
            else {
                translateAmt = 0;
                scaleAmt = -ev.detail.scrollTop / _this.headerHeight + 1;
            }
            _this.domCtrl.write(function () {
                _this.renderer.setStyle(_this.header, "transform", "translate3d(0," +
                    translateAmt +
                    "px,0) scale(" +
                    scaleAmt +
                    "," +
                    scaleAmt +
                    ")");
                _this.renderer.setStyle(_this.mainContent, "transform", "translate3d(0, " + -ev.detail.scrollTop + "px, 0");
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("parallaxHeader"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ParallaxHeaderDirective.prototype, "imagePath", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])("parallaxHeight"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], ParallaxHeaderDirective.prototype, "parallaxHeight", void 0);
    ParallaxHeaderDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: "[parallaxHeader]",
            host: {
                "(ionScroll)": "onContentScroll($event)"
            }
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["Renderer2"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["DomController"]])
    ], ParallaxHeaderDirective);
    return ParallaxHeaderDirective;
}());



/***/ }),

/***/ "./src/app/pages/detail-feed/detail-feed.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/detail-feed/detail-feed.module.ts ***!
  \*********************************************************/
/*! exports provided: DetailFeedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailFeedPageModule", function() { return DetailFeedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _detail_feed_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./detail-feed.page */ "./src/app/pages/detail-feed/detail-feed.page.ts");
/* harmony import */ var _directives_parallax_header_directive__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../directives/parallax-header.directive */ "./src/app/directives/parallax-header.directive.ts");








var routes = [
    {
        path: "",
        component: _detail_feed_page__WEBPACK_IMPORTED_MODULE_6__["DetailFeedPage"]
    }
];
var DetailFeedPageModule = /** @class */ (function () {
    function DetailFeedPageModule() {
    }
    DetailFeedPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_detail_feed_page__WEBPACK_IMPORTED_MODULE_6__["DetailFeedPage"], _directives_parallax_header_directive__WEBPACK_IMPORTED_MODULE_7__["ParallaxHeaderDirective"]]
        })
    ], DetailFeedPageModule);
    return DetailFeedPageModule;
}());



/***/ }),

/***/ "./src/app/pages/detail-feed/detail-feed.page.html":
/*!*********************************************************!*\
  !*** ./src/app/pages/detail-feed/detail-feed.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button\n        routerLink=\"/menu/main/tabs/home\"\n        routerDirection=\"back\"\n      ></ion-back-button>\n    </ion-buttons>\n    <ion-title>Comentarios</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content\n  [scrollEvents]=\"true\"\n  parallaxHeader=\"https://ununsplash.imgix.net/photo-1421091242698-34f6ad7fc088?fit=crop&fm=jpg&h=650&q=75&w=950\"\n  [parallaxHeight]=\"300\"\n>\n  <div class=\"main-content\">\n    <ion-grid fixed>\n      <ion-row>\n        <ion-col size=\"12\">\n          <div style=\"display: flex;\">\n            <ion-col size=\"2\">\n              <ion-avatar>\n                <img\n                  src=\"https://images.unsplash.com/photo-1510227272981-87123e259b17?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=3759e09a5b9fbe53088b23c615b6312e\"\n                />\n              </ion-avatar>\n            </ion-col>\n            <ion-col\n              style=\"background-color: aliceblue; margin-left: 5px; border-radius: 5%;\"\n            >\n              <ion-label>\n                <p>\n                  is simply dummy text of the printing and typesetting industry.\n                  Lorem Ipsum has been the industry's standard dummy\n                </p>\n              </ion-label>\n            </ion-col>\n          </div>\n        </ion-col>\n        <ion-col size=\"12\">\n          <div style=\"display:flex; flex-direction: row-reverse;\">\n            <ion-col size=\"2\">\n              <ion-avatar>\n                <img src=\"https://randomuser.me/api/portraits/women/82.jpg\" />\n              </ion-avatar>\n            </ion-col>\n            <ion-col\n              style=\"background-color: aliceblue; margin-right: 5px; border-radius: 5%;\"\n            >\n              <ion-label>\n                <p>\n                  is simply dummy text of the printing and typesetting industry.\n                  Lorem Ipsum has been the industry's standard dummy\n                </p>\n              </ion-label>\n            </ion-col>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/detail-feed/detail-feed.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/detail-feed/detail-feed.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-content {\n  background-color: #fff;\n  padding: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9lZHVhcmRvL0RvY3VtZW50cy9hcHAtc2VydmVyL2NvbGFicmEvY2FuYWwtbW9iaWxlLTQtY2FwYWNpdG9yL3NyYy9hcHAvcGFnZXMvZGV0YWlsLWZlZWQvZGV0YWlsLWZlZWQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usc0JBQXNCO0VBQ3RCLGFBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFpbC1mZWVkL2RldGFpbC1mZWVkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluLWNvbnRlbnQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICBwYWRkaW5nOiAyMHB4O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/detail-feed/detail-feed.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/detail-feed/detail-feed.page.ts ***!
  \*******************************************************/
/*! exports provided: DetailFeedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailFeedPage", function() { return DetailFeedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DetailFeedPage = /** @class */ (function () {
    function DetailFeedPage() {
    }
    DetailFeedPage.prototype.ngOnInit = function () { };
    DetailFeedPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-detail-feed",
            template: __webpack_require__(/*! ./detail-feed.page.html */ "./src/app/pages/detail-feed/detail-feed.page.html"),
            styles: [__webpack_require__(/*! ./detail-feed.page.scss */ "./src/app/pages/detail-feed/detail-feed.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DetailFeedPage);
    return DetailFeedPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-detail-feed-detail-feed-module.js.map