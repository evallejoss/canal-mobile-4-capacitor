(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-share-share-module"],{

/***/ "./src/app/pages/share/share.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/share/share.module.ts ***!
  \*********************************************/
/*! exports provided: SharePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharePageModule", function() { return SharePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _share_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./share.page */ "./src/app/pages/share/share.page.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");








var routes = [
    {
        path: "",
        component: _share_page__WEBPACK_IMPORTED_MODULE_6__["SharePage"]
    }
];
var SharePageModule = /** @class */ (function () {
    function SharePageModule() {
    }
    SharePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]
            ],
            declarations: [_share_page__WEBPACK_IMPORTED_MODULE_6__["SharePage"]]
        })
    ], SharePageModule);
    return SharePageModule;
}());



/***/ }),

/***/ "./src/app/pages/share/share.page.html":
/*!*********************************************!*\
  !*** ./src/app/pages/share/share.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button animated=\"true\" defaulHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Compartir</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-item-divider class=\"colorDivider\" sticky=\"true\">\n    <ion-searchbar\n      (ionChange)=\"buscar($event)\"\n      placeholder=\"Buscar\"\n    ></ion-searchbar>\n  </ion-item-divider>\n  <ion-list force-overscroll=\"true\" class=\"ion-padding-top\">\n    <ion-item\n      lines=\"full\"\n      *ngFor=\"let item of test | filtro: textoBuscar:'title'\"\n      lines=\"none\"\n    >\n      <ion-avatar>\n        <img\n          src=\"https://images.unsplash.com/photo-1510227272981-87123e259b17?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=3759e09a5b9fbe53088b23c615b6312e\"\n        />\n      </ion-avatar>\n      <ion-label class=\"ion-margin-start\">\n        {{ item.title }}\n      </ion-label>\n      <ion-checkbox></ion-checkbox>\n    </ion-item>\n  </ion-list>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/share/share.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/share/share.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".colorDivider {\n  --background: trasparent; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9lZHVhcmRvL0RvY3VtZW50cy9hcHAtc2VydmVyL2NvbGFicmEvY2FuYWwtbW9iaWxlLTQtY2FwYWNpdG9yL3NyYy9hcHAvcGFnZXMvc2hhcmUvc2hhcmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usd0JBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NoYXJlL3NoYXJlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb2xvckRpdmlkZXIge1xuICAtLWJhY2tncm91bmQ6IHRyYXNwYXJlbnQ7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/share/share.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/share/share.page.ts ***!
  \*******************************************/
/*! exports provided: SharePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharePage", function() { return SharePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SharePage = /** @class */ (function () {
    function SharePage() {
        this.test = [
            { title: "Conrado Trueba Dulce" },
            { title: "Celeste Brochero Frenes" },
            { title: "Gustavo Sorzano Cavada" },
            { title: "Klelia Cobian Echart" },
            { title: "Viannette Zapatero Parrondo" },
            { title: "Juliana Galarreta Fontiveros" },
            { title: "Anayda Bueras Sosa" },
            { title: "Iñaki Blanco Tovilleja" },
            { title: "Pilar Varez Rionda" },
            { title: "Ponciano Arena Rivacoba" }
        ];
        this.textoBuscar = "";
    }
    SharePage.prototype.ngOnInit = function () { };
    SharePage.prototype.buscar = function (event) {
        this.textoBuscar = event.detail.value;
    };
    SharePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-share",
            template: __webpack_require__(/*! ./share.page.html */ "./src/app/pages/share/share.page.html"),
            styles: [__webpack_require__(/*! ./share.page.scss */ "./src/app/pages/share/share.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SharePage);
    return SharePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-share-share-module.js.map