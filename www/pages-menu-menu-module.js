(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-menu-menu-module"],{

/***/ "./src/app/pages/menu/Menu.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/menu/Menu.page.ts ***!
  \*****************************************/
/*! exports provided: MenuPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPage", function() { return MenuPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_FirebaseComponentConfig_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/FirebaseComponentConfig.service */ "./src/app/services/FirebaseComponentConfig.service.ts");
/* harmony import */ var _components_profile_profile_dinamic_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/profile/profile-dinamic.component */ "./src/app/components/profile/profile-dinamic.component.ts");
/* harmony import */ var _services_Menu_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/Menu.service */ "./src/app/services/Menu.service.ts");







var MenuPage = /** @class */ (function () {
    function MenuPage(router, menu, fireService, resolver, menuSrv) {
        var _this = this;
        this.router = router;
        this.menu = menu;
        this.fireService = fireService;
        this.resolver = resolver;
        this.menuSrv = menuSrv;
        this.selectedPath = "";
        this.items = [];
        this.header = [];
        var componentRef;
        this.fireService.getPorfile().subscribe(function (res) {
            var data = _this.fireService.getProcessSteps(res);
            _this.container.clear();
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var item = data_1[_i];
                var factory = _this.resolver.resolveComponentFactory(_components_profile_profile_dinamic_component__WEBPACK_IMPORTED_MODULE_5__["ProfileOneComponent"]);
                componentRef = _this.container.createComponent(factory);
                componentRef.instance.data = item;
            }
        });
        this.router.events.subscribe(function (event) {
            if (event && event.url) {
                _this.selectedPath = event.url;
            }
        });
    }
    MenuPage.prototype.ngOnInit = function () {
        var _this = this;
        this.menuSrv.build().subscribe(function (result) {
            _this.header = result.data.menu.sideMenu.header;
            _this.items = result.data.menu.sideMenu.items;
        });
    };
    MenuPage.prototype.redirectView = function (view) {
        this.router.navigateByUrl("/menu/main/tabs/" + view);
    };
    MenuPage.prototype.closeMenu = function (view) {
        var _this = this;
        this.menu
            .close("first")
            .then(function (_) {
            _this.router.navigateByUrl("/menu/main/tabs/perfil");
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("processContainer", { read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewContainerRef"] }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], MenuPage.prototype, "container", void 0);
    MenuPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-menu",
            template: __webpack_require__(/*! ./menu.page.html */ "./src/app/pages/menu/menu.page.html"),
            styles: [__webpack_require__(/*! ./menu.page.scss */ "./src/app/pages/menu/menu.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"],
            _services_FirebaseComponentConfig_service__WEBPACK_IMPORTED_MODULE_4__["FirebaseComponentConfig"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ComponentFactoryResolver"],
            _services_Menu_service__WEBPACK_IMPORTED_MODULE_6__["MenuService"]])
    ], MenuPage);
    return MenuPage;
}());



/***/ }),

/***/ "./src/app/pages/menu/menu.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/menu/menu.module.ts ***!
  \*******************************************/
/*! exports provided: MenuPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageModule", function() { return MenuPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Menu_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Menu.page */ "./src/app/pages/menu/Menu.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "./node_modules/@fortawesome/angular-fontawesome/fesm5/angular-fontawesome.js");








var routes = [
    {
        path: "",
        component: _Menu_page__WEBPACK_IMPORTED_MODULE_5__["MenuPage"],
        children: [
            {
                path: "main",
                loadChildren: "../main/main.module#MainPageModule"
            },
            {
                path: "main/tabs/perfil",
                loadChildren: "../perfil/perfil.module#PerfilPageModule"
            },
            {
                path: "main/tabs/beneficios",
                loadChildren: "../beneficios/beneficios.module#BeneficiosPageModule"
            }
        ]
    }
];
var MenuPageModule = /** @class */ (function () {
    function MenuPageModule() {
    }
    MenuPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__["FontAwesomeModule"]
            ],
            declarations: [_Menu_page__WEBPACK_IMPORTED_MODULE_5__["MenuPage"]]
        })
    ], MenuPageModule);
    return MenuPageModule;
}());



/***/ }),

/***/ "./src/app/pages/menu/menu.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/menu/menu.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-split-pane>\n  <ion-menu type=\"push\" contentId=\"principal\" side=\"end\" menuId=\"first\">\n    <div #processContainer></div>\n    <div class=\"container\">\n      <div class=\"ion-text-cente nameUser\">\n        <h3>Hola, {{ header.lastname }}</h3>\n      </div>\n      <div\n        class=\"ion-text-center nameUser textViewProfile\"\n        (click)=\"closeMenu()\"\n        routerDirection=\"root\"\n      >\n        Ver mi perfil\n      </div>\n      <hr class=\"headerBorderFeed\" />\n    </div>\n    <ion-content force-overscroll=\"true\" class=\"ion-padding-top\">\n      <ion-list>\n        <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let item of items\">\n          <ion-item\n            (click)=\"redirectView(item.action.routerDirection)\"\n            routerDirection=\"root\"\n            [class.active-item]=\"\n              selectedPath.startsWith(item.action.routerDirection)\n            \"\n            lines=\"none\"\n          >\n            <fa-icon\n              [icon]=\"item.icon\"\n              color=\"primary\"\n              slot=\"start\"\n              style=\"font-size: 1.1rem;\"\n            ></fa-icon>\n            <ion-label>{{ item.name }}</ion-label>\n          </ion-item>\n        </ion-menu-toggle>\n      </ion-list>\n    </ion-content>\n  </ion-menu>\n\n  <ion-router-outlet id=\"principal\" main></ion-router-outlet>\n</ion-split-pane>\n"

/***/ }),

/***/ "./src/app/pages/menu/menu.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/menu/menu.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".nameUser h3 {\n  font-weight: 400;\n  font-size: 1rem;\n  line-height: 0; }\n\n.textViewProfile {\n  font-weight: 300;\n  line-height: 1.7;\n  color: var(--ion-color-secondary); }\n\n.container {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n  text-align: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9lZHVhcmRvL0RvY3VtZW50cy9hcHAtc2VydmVyL2NvbGFicmEvY2FuYWwtbW9iaWxlLTQtY2FwYWNpdG9yL3NyYy9hcHAvcGFnZXMvbWVudS9tZW51LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVJLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsY0FBYyxFQUFBOztBQUlsQjtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsaUNBQWlDLEVBQUE7O0FBR25DO0VBQ0Usb0JBQWE7RUFBYixhQUFhO0VBQ2IsNEJBQXNCO0VBQXRCLDZCQUFzQjtVQUF0QixzQkFBc0I7RUFDdEIsd0JBQXVCO1VBQXZCLHVCQUF1QjtFQUN2QixrQkFBa0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21lbnUvbWVudS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmFtZVVzZXIge1xuICBoMyB7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgbGluZS1oZWlnaHQ6IDA7XG4gIH1cbn1cblxuLnRleHRWaWV3UHJvZmlsZSB7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjc7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbn1cblxuLmNvbnRhaW5lcntcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbiJdfQ== */"

/***/ })

}]);
//# sourceMappingURL=pages-menu-menu-module.js.map