(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~home-home-module~main-main-module"],{

/***/ "./src/app/pages/main/Main.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/main/Main.page.ts ***!
  \*****************************************/
/*! exports provided: MainPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPage", function() { return MainPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @capacitor/core */ "./node_modules/@capacitor/core/dist/esm/index.js");
/* harmony import */ var _services_FirebaseSocket__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/FirebaseSocket */ "./src/app/services/FirebaseSocket.ts");
/* harmony import */ var _services_CacheConfigUser_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/CacheConfigUser.service */ "./src/app/services/CacheConfigUser.service.ts");
/* harmony import */ var _services_CacheApi_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/CacheApi.service */ "./src/app/services/CacheApi.service.ts");
/* harmony import */ var _services_Home_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/Home.service */ "./src/app/services/Home.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services_Menu_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/Menu.service */ "./src/app/services/Menu.service.ts");




var LocalNotifications = _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].LocalNotifications;






var MainPage = /** @class */ (function () {
    function MainPage(menuCtrl, backTask, configUser, cacheApi, srvHome, socketFrs, menuSrv) {
        this.menuCtrl = menuCtrl;
        this.backTask = backTask;
        this.configUser = configUser;
        this.cacheApi = cacheApi;
        this.srvHome = srvHome;
        this.socketFrs = socketFrs;
        this.menuSrv = menuSrv;
        this.notification = 0;
        this.newNotification = new rxjs__WEBPACK_IMPORTED_MODULE_8__["Subject"]();
        this.menu = [];
    }
    MainPage.prototype.ngOnInit = function () {
        var _this = this;
        this.changeFeed();
        LocalNotifications.addListener("localNotificationActionPerformed", function (action) { });
        this.menuSrv.build().subscribe(function (result) {
            _this.menu = result.data.menu.footerMenu.slice(1);
            console.log(_this.menu);
        });
    };
    MainPage.prototype.changeFeed = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var uuid;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.configUser.getCacheConfig()];
                    case 1:
                        uuid = _a.sent();
                        this.backTask.socketFeed$(uuid).subscribe(function (resp) {
                            _this.updateFeed(resp.payload.data().api.feed.quantity);
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    MainPage.prototype.toggleMenu = function () {
        this.menuCtrl.open("end");
        this.menuCtrl.toggle();
    };
    MainPage.prototype.updateFeed = function (notification) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.notificationUpdate(notification);
                return [2 /*return*/];
            });
        });
    };
    MainPage.prototype.notificationUpdate = function (notification) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                if (notification > 0) {
                    LocalNotifications.schedule({
                        notifications: [
                            {
                                title: "Hay " + notification + " contenidos nuevos",
                                body: "En el muro",
                                id: 2,
                                schedule: { at: new Date(Date.now() + 1000 * 1) },
                                sound: "assets/sound/1111.aiff",
                                attachments: null,
                                actionTypeId: "",
                                extra: null
                            }
                        ]
                    });
                    this.notification = notification;
                }
                return [2 /*return*/];
            });
        });
    };
    MainPage.prototype.onPressTab = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var uuid;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.notification > 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.cacheApi.clearCacheApiGroup("get", "feed")];
                    case 1:
                        _a.sent();
                        this.srvHome.content();
                        return [4 /*yield*/, this.configUser.getCacheConfig()];
                    case 2:
                        uuid = _a.sent();
                        this.socketFrs.socketFeedUpdate$(uuid);
                        this.notification = 0;
                        this.newNotification.next(this.notification);
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])("myTabs"),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonTabs"])
    ], MainPage.prototype, "tabRef", void 0);
    MainPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-main",
            template: __webpack_require__(/*! ./main.page.html */ "./src/app/pages/main/main.page.html"),
            styles: [__webpack_require__(/*! ./main.page.scss */ "./src/app/pages/main/main.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"],
            _services_FirebaseSocket__WEBPACK_IMPORTED_MODULE_4__["FirebaseSocket"],
            _services_CacheConfigUser_service__WEBPACK_IMPORTED_MODULE_5__["CacheConfigUser"],
            _services_CacheApi_service__WEBPACK_IMPORTED_MODULE_6__["CacheServiceApiRest"],
            _services_Home_service__WEBPACK_IMPORTED_MODULE_7__["HomeService"],
            _services_FirebaseSocket__WEBPACK_IMPORTED_MODULE_4__["FirebaseSocket"],
            _services_Menu_service__WEBPACK_IMPORTED_MODULE_9__["MenuService"]])
    ], MainPage);
    return MainPage;
}());



/***/ }),

/***/ "./src/app/pages/main/main.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/main/main.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs #myTabs class=\"tabColor\">\n  <ion-tab-bar slot=\"bottom\">\n    <ion-tab-button tab=\"home\" (click)=\"onPressTab()\">\n      <fa-icon\n        style=\"font-size: 1.3rem;\"\n        [icon]=\"['fas', 'rss-square']\"\n        class=\"ion-margin-top\"\n      ></fa-icon>\n      <ion-badge color=\"secondary\" [hidden]=\"notification === 0\">{{\n        notification\n      }}</ion-badge>\n      <ion-label style=\"font-size: 0.7rem;\">Home</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button tab=\"beneficios\" *ngFor=\"let item of menu\">\n      <fa-icon\n        style=\"font-size: 1.3rem;\"\n        [icon]=\"item.icon\"\n        class=\"ion-margin-top\"\n      ></fa-icon>\n      <ion-label style=\"font-size: 0.7rem;\">{{ item.name }}</ion-label>\n    </ion-tab-button>\n\n    <ion-tab-button (click)=\"toggleMenu()\">\n      <fa-icon\n        style=\"font-size: 1.3rem;\"\n        [icon]=\"['fas', 'bars']\"\n        class=\"ion-margin-top\"\n      ></fa-icon>\n      <ion-label style=\"font-size: 0.7rem;\">Menu</ion-label>\n    </ion-tab-button>\n  </ion-tab-bar>\n</ion-tabs>\n"

/***/ }),

/***/ "./src/app/pages/main/main.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/main/main.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".tabColor {\n  --ion-tab-bar-background: var(--colorTab);\n  --ion-tab-bar-color: var(--ion-color-light);\n  --ion-tab-bar-color-activated: var(--ion-color-light); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9lZHVhcmRvL0RvY3VtZW50cy9hcHAtc2VydmVyL2NvbGFicmEvY2FuYWwtbW9iaWxlLTQtY2FwYWNpdG9yL3NyYy9hcHAvcGFnZXMvbWFpbi9tYWluLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHlDQUF5QjtFQUN6QiwyQ0FBb0I7RUFDcEIscURBQThCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9tYWluL21haW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRhYkNvbG9yIHtcbiAgLS1pb24tdGFiLWJhci1iYWNrZ3JvdW5kOiB2YXIoLS1jb2xvclRhYik7XG4gIC0taW9uLXRhYi1iYXItY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gIC0taW9uLXRhYi1iYXItY29sb3ItYWN0aXZhdGVkOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/services/CacheConfigUser.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/CacheConfigUser.service.ts ***!
  \*****************************************************/
/*! exports provided: CacheConfigUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CacheConfigUser", function() { return CacheConfigUser; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _config_cacheDB__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../config/cacheDB */ "./src/app/config/cacheDB.ts");



var CacheConfigUser = /** @class */ (function () {
    function CacheConfigUser() {
    }
    // verifica si esta en cache el request y si expiro la fecha.
    // Inmediatamente realiza la llamada al servicio y lo remplaza
    CacheConfigUser.prototype.setCacheConfig = function (dataUser) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var object, resp;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, _config_cacheDB__WEBPACK_IMPORTED_MODULE_2__["default"]
                            .table("config")
                            .where("user")
                            .equals(dataUser.uuid)
                            .limit(1)
                            .toArray()];
                    case 1:
                        object = _a.sent();
                        if (!(object.length == 0)) return [3 /*break*/, 3];
                        return [4 /*yield*/, _config_cacheDB__WEBPACK_IMPORTED_MODULE_2__["default"].table("config").put({
                                user: dataUser.uuid,
                                dataUser: dataUser
                            })];
                    case 2:
                        resp = _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/, Promise.resolve()];
                }
            });
        });
    };
    CacheConfigUser.prototype.getCacheConfig = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var object;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, _config_cacheDB__WEBPACK_IMPORTED_MODULE_2__["default"].table("config").toArray()];
                    case 1:
                        object = _a.sent();
                        if (object.length > 0) {
                            return [2 /*return*/, Promise.resolve(object[0].user)];
                        }
                        return [2 /*return*/, Promise.reject("no existe config de usuario")];
                }
            });
        });
    };
    CacheConfigUser = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CacheConfigUser);
    return CacheConfigUser;
}());



/***/ }),

/***/ "./src/app/services/FirebaseSocket.ts":
/*!********************************************!*\
  !*** ./src/app/services/FirebaseSocket.ts ***!
  \********************************************/
/*! exports provided: FirebaseSocket */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseSocket", function() { return FirebaseSocket; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__);



var FirebaseSocket = /** @class */ (function () {
    function FirebaseSocket(db) {
        this.db = db;
    }
    FirebaseSocket.prototype.socketFeed$ = function (uuid) {
        return this.db
            .collection("apichange")
            .doc(uuid)
            .snapshotChanges();
    };
    FirebaseSocket.prototype.socketFeedUpdate$ = function (uuid) {
        this.db
            .collection("apichange")
            .doc(uuid)
            .update({
            api: {
                feed: {
                    quantity: 0,
                    date: new Date().getDate()
                }
            }
        });
    };
    FirebaseSocket = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"]])
    ], FirebaseSocket);
    return FirebaseSocket;
}());



/***/ }),

/***/ "./src/app/services/Home.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/Home.service.ts ***!
  \******************************************/
/*! exports provided: HomeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeService", function() { return HomeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _config_Config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config/Config */ "./src/app/config/Config.ts");
/* harmony import */ var _CacheApi_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./CacheApi.service */ "./src/app/services/CacheApi.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _OfflineManagerService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./OfflineManagerService */ "./src/app/services/OfflineManagerService.ts");
/* harmony import */ var _Network_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./Network.service */ "./src/app/services/Network.service.ts");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_10__);











var Feed = graphql_tag__WEBPACK_IMPORTED_MODULE_10___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n  query Feed($start: Int!) {\n    feed(start: $start, take: 20) {\n      name\n      image\n      type\n    }\n  }\n"], ["\n  query Feed($start: Int!) {\n    feed(start: $start, take: 20) {\n      name\n      image\n      type\n    }\n  }\n"])));
var HomeService = /** @class */ (function () {
    function HomeService(http, cache, offline, newteork, apollo) {
        this.http = http;
        this.cache = cache;
        this.offline = offline;
        this.newteork = newteork;
        this.apollo = apollo;
        this.status = new rxjs__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"]([]);
        this.content();
    }
    HomeService.prototype.paginationFeed = function (group, type, pagination, publicDate) {
        var key = "/assets/data/feed.json";
        return this.cache.cacheGetApi(type, key, group);
    };
    HomeService.prototype.content = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var pagination, publicDate, key, data;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        pagination = 0;
                        publicDate = 0;
                        key = "/assets/data/feed.json";
                        return [4 /*yield*/, this.cache.cacheGetApi("get", key, "feed")];
                    case 1:
                        data = _a.sent();
                        this.status.next(data);
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeService.prototype.sendPost = function (data) {
        var _this = this;
        if (this.newteork.getCurrentNetworkStatus() == _Network_service__WEBPACK_IMPORTED_MODULE_8__["ConnectionStatus"].Offline) {
            return this.offline.storeRequest(_config_Config__WEBPACK_IMPORTED_MODULE_3__["Config"].URL + "\"/enviar/feed\"", "post", data);
        }
        else {
            return this.http.post(_config_Config__WEBPACK_IMPORTED_MODULE_3__["Config"].URL + "\"/enviar/feed\"", data).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(function (error) {
                _this.offline.storeRequest(_config_Config__WEBPACK_IMPORTED_MODULE_3__["Config"].URL + "\"/enviar/megusta\"", "post", data);
                throw new Error(error);
            }));
        }
    };
    HomeService.prototype.onfeed$ = function (start) {
        return this.apollo.watchQuery({
            query: Feed,
            variables: {
                start: start
            }
        }).valueChanges;
    };
    HomeService.prototype.onFeedChange$ = function () {
        return this.status.asObservable();
    };
    HomeService.prototype.onFeedStatus$ = function () {
        return this.status.getValue();
    };
    HomeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _CacheApi_service__WEBPACK_IMPORTED_MODULE_4__["CacheServiceApiRest"],
            _OfflineManagerService__WEBPACK_IMPORTED_MODULE_7__["OfflineManagerService"],
            _Network_service__WEBPACK_IMPORTED_MODULE_8__["NetworkService"],
            apollo_angular__WEBPACK_IMPORTED_MODULE_9__["Apollo"]])
    ], HomeService);
    return HomeService;
}());

var templateObject_1;


/***/ })

}]);
//# sourceMappingURL=default~home-home-module~main-main-module.js.map