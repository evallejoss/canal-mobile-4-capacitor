(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-modals-feed-create-feed-create-module"],{

/***/ "./src/app/services/Home.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/Home.service.ts ***!
  \******************************************/
/*! exports provided: HomeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeService", function() { return HomeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _config_Config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config/Config */ "./src/app/config/Config.ts");
/* harmony import */ var _CacheApi_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./CacheApi.service */ "./src/app/services/CacheApi.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _OfflineManagerService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./OfflineManagerService */ "./src/app/services/OfflineManagerService.ts");
/* harmony import */ var _Network_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./Network.service */ "./src/app/services/Network.service.ts");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ngApollo.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_10__);











var Feed = graphql_tag__WEBPACK_IMPORTED_MODULE_10___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n  query Feed($start: Int!) {\n    feed(start: $start, take: 20) {\n      name\n      image\n      type\n    }\n  }\n"], ["\n  query Feed($start: Int!) {\n    feed(start: $start, take: 20) {\n      name\n      image\n      type\n    }\n  }\n"])));
var HomeService = /** @class */ (function () {
    function HomeService(http, cache, offline, newteork, apollo) {
        this.http = http;
        this.cache = cache;
        this.offline = offline;
        this.newteork = newteork;
        this.apollo = apollo;
        this.status = new rxjs__WEBPACK_IMPORTED_MODULE_5__["BehaviorSubject"]([]);
        this.content();
    }
    HomeService.prototype.paginationFeed = function (group, type, pagination, publicDate) {
        var key = "/assets/data/feed.json";
        return this.cache.cacheGetApi(type, key, group);
    };
    HomeService.prototype.content = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var pagination, publicDate, key, data;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        pagination = 0;
                        publicDate = 0;
                        key = "/assets/data/feed.json";
                        return [4 /*yield*/, this.cache.cacheGetApi("get", key, "feed")];
                    case 1:
                        data = _a.sent();
                        this.status.next(data);
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeService.prototype.sendPost = function (data) {
        var _this = this;
        if (this.newteork.getCurrentNetworkStatus() == _Network_service__WEBPACK_IMPORTED_MODULE_8__["ConnectionStatus"].Offline) {
            return this.offline.storeRequest(_config_Config__WEBPACK_IMPORTED_MODULE_3__["Config"].URL + "\"/enviar/feed\"", "post", data);
        }
        else {
            return this.http.post(_config_Config__WEBPACK_IMPORTED_MODULE_3__["Config"].URL + "\"/enviar/feed\"", data).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["catchError"])(function (error) {
                _this.offline.storeRequest(_config_Config__WEBPACK_IMPORTED_MODULE_3__["Config"].URL + "\"/enviar/megusta\"", "post", data);
                throw new Error(error);
            }));
        }
    };
    HomeService.prototype.onfeed$ = function (start) {
        return this.apollo.watchQuery({
            query: Feed,
            variables: {
                start: start
            }
        }).valueChanges;
    };
    HomeService.prototype.onFeedChange$ = function () {
        return this.status.asObservable();
    };
    HomeService.prototype.onFeedStatus$ = function () {
        return this.status.getValue();
    };
    HomeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _CacheApi_service__WEBPACK_IMPORTED_MODULE_4__["CacheServiceApiRest"],
            _OfflineManagerService__WEBPACK_IMPORTED_MODULE_7__["OfflineManagerService"],
            _Network_service__WEBPACK_IMPORTED_MODULE_8__["NetworkService"],
            apollo_angular__WEBPACK_IMPORTED_MODULE_9__["Apollo"]])
    ], HomeService);
    return HomeService;
}());

var templateObject_1;


/***/ })

}]);
//# sourceMappingURL=pages-modals-feed-create-feed-create-module.js.map