(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["beneficios-beneficios-module"],{

/***/ "./src/app/pages/beneficios/Beneficios.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/beneficios/Beneficios.page.ts ***!
  \*****************************************************/
/*! exports provided: BeneficiosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeneficiosPage", function() { return BeneficiosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var BeneficiosPage = /** @class */ (function () {
    function BeneficiosPage() {
        this.test = [
            { title: "test1" },
            { title: "test2" },
            { title: "test3" },
            { title: "test4" }
        ];
        this.textoBuscar = "";
    }
    BeneficiosPage.prototype.ngOnInit = function () { };
    BeneficiosPage.prototype.buscar = function (event) {
        this.textoBuscar = event.detail.value;
    };
    BeneficiosPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-beneficios",
            template: __webpack_require__(/*! ./beneficios.page.html */ "./src/app/pages/beneficios/beneficios.page.html"),
            styles: [__webpack_require__(/*! ./beneficios.page.scss */ "./src/app/pages/beneficios/beneficios.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], BeneficiosPage);
    return BeneficiosPage;
}());



/***/ }),

/***/ "./src/app/pages/beneficios/beneficios.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/beneficios/beneficios.module.ts ***!
  \*******************************************************/
/*! exports provided: BeneficiosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeneficiosPageModule", function() { return BeneficiosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Beneficios_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Beneficios.page */ "./src/app/pages/beneficios/Beneficios.page.ts");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");







var routes = [
    {
        path: "",
        component: _Beneficios_page__WEBPACK_IMPORTED_MODULE_5__["BeneficiosPage"]
    }
];
var BeneficiosPageModule = /** @class */ (function () {
    function BeneficiosPageModule() {
    }
    BeneficiosPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__["PipesModule"]
            ],
            declarations: [_Beneficios_page__WEBPACK_IMPORTED_MODULE_5__["BeneficiosPage"]]
        })
    ], BeneficiosPageModule);
    return BeneficiosPageModule;
}());



/***/ }),

/***/ "./src/app/pages/beneficios/beneficios.page.html":
/*!*******************************************************!*\
  !*** ./src/app/pages/beneficios/beneficios.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaulHref=\"/\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Beneficios</ion-title>\n  </ion-toolbar>\n  <ion-searchbar\n    (ionChange)=\"buscar($event)\"\n    placeholder=\"Buscar\"\n  ></ion-searchbar>\n</ion-header>\n<ion-content>\n  <ion-list>\n    <ion-item\n      lines=\"full\"\n      *ngFor=\"let item of test | filtro: textoBuscar:'title'\"\n    >\n      {{ item.title }}\n    </ion-item>\n  </ion-list>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/beneficios/beneficios.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/beneficios/beneficios.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2JlbmVmaWNpb3MvYmVuZWZpY2lvcy5wYWdlLnNjc3MifQ== */"

/***/ })

}]);
//# sourceMappingURL=beneficios-beneficios-module.js.map