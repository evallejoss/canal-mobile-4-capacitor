(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["perfil-perfil-module"],{

/***/ "./src/app/pages/perfil/Perfil.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/perfil/Perfil.page.ts ***!
  \*********************************************/
/*! exports provided: PerfilPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPage", function() { return PerfilPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");





var PerfilPage = /** @class */ (function () {
    function PerfilPage(sanitizer, menu) {
        this.sanitizer = sanitizer;
        this.menu = menu;
        this.imgHeaderCard = "../assets/img/Bg-login-alsea.svg";
        this.imgAvatar = "../assets/img/profile1.jpg";
        this.profile = [
            { icon: "send", name: "josefa@starbucks.cl", profile: true },
            { icon: "globe", name: "Providencia", profile: true },
            { icon: "phone-portrait", name: "(+569) 78934567", profile: true },
            {
                icon: "star-outline",
                name: "Division Starbucks Providencia",
                profile: true
            }
        ];
        this.messages = [
            {
                user: "Simon",
                createdAt: 1554090856000,
                msg: "Hola todo bien en el local?"
            },
            {
                user: "Josefa",
                createdAt: 1554090956000,
                msg: "excelente todos trabajando ya"
            },
            {
                user: "Simon",
                createdAt: 1554091056000,
                msg: "Muy Bien gracias!!!"
            }
        ];
        this.currentUser = "Simon";
    }
    PerfilPage.prototype.ngOnInit = function () { };
    PerfilPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-perfil",
            template: __webpack_require__(/*! ./perfil.page.html */ "./src/app/pages/perfil/perfil.page.html"),
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])("fadein", [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["state"])("load", Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])("void => *", [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])("900ms ease-out", Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1 }))
                    ])
                ])
            ],
            styles: [__webpack_require__(/*! ./perfil.page.scss */ "./src/app/pages/perfil/perfil.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]])
    ], PerfilPage);
    return PerfilPage;
}());



/***/ }),

/***/ "./src/app/pages/perfil/perfil.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/perfil/perfil.module.ts ***!
  \***********************************************/
/*! exports provided: PerfilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPageModule", function() { return PerfilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _Perfil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Perfil.page */ "./src/app/pages/perfil/Perfil.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








var routes = [
    {
        path: "",
        component: _Perfil_page__WEBPACK_IMPORTED_MODULE_6__["PerfilPage"]
    }
];
var PerfilPageModule = /** @class */ (function () {
    function PerfilPageModule() {
    }
    PerfilPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
                _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
            ],
            declarations: [_Perfil_page__WEBPACK_IMPORTED_MODULE_6__["PerfilPage"]]
        })
    ], PerfilPageModule);
    return PerfilPageModule;
}());



/***/ }),

/***/ "./src/app/pages/perfil/perfil.page.html":
/*!***********************************************!*\
  !*** ./src/app/pages/perfil/perfil.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title text-capitalize>Mi Perfil</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n    <ion-row class=\"perfilImg ion-margin-top\">\n        <ion-col size=\"12\">\n          <div>\n            <ion-avatar class=\"image-center\">\n              <img src=\"./assets/img/profile1.jpg\" @fadein />\n            </ion-avatar>\n            <div class=\"camera\">\n                <ion-button fill=\"clear\" color=\"secondary\" size=\"large\">\n                  <ion-icon color=\"secondary\" size=\"large\" name=\"camera\"></ion-icon>\n                </ion-button>\n            </div>\n            <div class=\"dataPerfil\">\n              <h2>Josefa Casanova</h2>\n              <p>Marketing</p>\n            </div>\n          </div>\n        </ion-col>\n    </ion-row>\n    <hr class=\"headerBorderFeed\"/>\n  <ion-list *ngFor=\"let item of profile\" class=\"ion-margin-top\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"start\" [name]=\"item.icon\"></ion-icon>\n      <ion-label>{{ item.name }}</ion-label>\n    </ion-item>\n  </ion-list>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/perfil/perfil.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/perfil/perfil.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".single-post-home {\n  margin-left: 0px;\n  margin-right: 0px;\n  padding: 0px;\n  width: 100%;\n  border-radius: 0;\n  margin-bottom: 0px;\n  margin-top: 1px; }\n\n.personalData {\n  background: white;\n  padding: 16px;\n  border: none;\n  border-radius: 10px; }\n\n.image-center {\n  margin: 0 auto;\n  width: 70px;\n  height: 70px;\n  border: 1px solid var(--ion-color-primary); }\n\n.perfilImg h2 {\n  font-size: 1.0rem;\n  font-weight: 900;\n  text-align: center;\n  margin-top: 1px; }\n\n.perfilImg p {\n  line-height: 0px;\n  font-size: 0.8rem;\n  font-weight: 400;\n  text-align: center; }\n\n.perfilImg .dataPerfil {\n  width: 100%;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -webkit-box-pack: center;\n          justify-content: center;\n  position: absolute;\n  margin-top: 8px; }\n\nion-avatar img {\n  border: 4px solid var(--ion-color-secondary); }\n\n.camera {\n  position: relative;\n  float: right; }\n\n.message {\n  padding: 10px;\n  border-radius: 10px;\n  margin-bottom: 4px;\n  white-space: pre-wrap; }\n\n.my-message {\n  background: var(--ion-color-tertiary);\n  color: #fff; }\n\n.other-message {\n  background: var(--ion-color-secondary);\n  color: #fff; }\n\n.time {\n  color: #dfdfdf;\n  float: right;\n  font-size: small; }\n\n.message-input {\n  width: 100%;\n  border: 1px solid var(--ion-color-medium);\n  border-radius: 10px;\n  background: #fff;\n  resize: none;\n  padding-left: 10px;\n  padding-right: 10px; }\n\n.msg-btn {\n  --padding-start: 0.5em;\n  --padding-end: 0.5em; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9lZHVhcmRvL0RvY3VtZW50cy9hcHAtc2VydmVyL2NvbGFicmEvY2FuYWwtbW9iaWxlLTQtY2FwYWNpdG9yL3NyYy9hcHAvcGFnZXMvcGVyZmlsL3BlcmZpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBSW5CO0VBQ0UsaUJBQWlCO0VBQ2YsYUFBYTtFQUNiLFlBQVk7RUFDWixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDRSxjQUFhO0VBQ2IsV0FBVztFQUNYLFlBQVk7RUFDWiwwQ0FBMkMsRUFBQTs7QUFHN0M7RUFFSSxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixlQUFlLEVBQUE7O0FBTG5CO0VBUUksZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsa0JBQWtCLEVBQUE7O0FBWHRCO0VBZUksV0FBVztFQUNYLG9CQUFhO0VBQWIsYUFBYTtFQUNiLDRCQUFzQjtFQUF0Qiw2QkFBc0I7VUFBdEIsc0JBQXNCO0VBQ3RCLHdCQUF1QjtVQUF2Qix1QkFBdUI7RUFDdkIsa0JBQWtCO0VBQ2xCLGVBQWUsRUFBQTs7QUFJbkI7RUFFSSw0Q0FBNEMsRUFBQTs7QUFJaEQ7RUFDRSxrQkFBa0I7RUFDbEIsWUFBWSxFQUFBOztBQUdkO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIscUJBQXFCLEVBQUE7O0FBR3ZCO0VBQ0UscUNBQXFDO0VBQ3JDLFdBQVcsRUFBQTs7QUFHYjtFQUNFLHNDQUFzQztFQUN0QyxXQUFXLEVBQUE7O0FBR2I7RUFDRSxjQUFjO0VBQ2QsWUFBWTtFQUNaLGdCQUFnQixFQUFBOztBQUdsQjtFQUNFLFdBQVc7RUFDWCx5Q0FBeUM7RUFDekMsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLHNCQUFnQjtFQUNoQixvQkFBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGVyZmlsL3BlcmZpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuc2luZ2xlLXBvc3QtaG9tZXtcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMHB4O1xuICAgIHBhZGRpbmc6IDBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICBtYXJnaW4tdG9wOiAxcHg7XG4gICAgLy9cbn1cblxuLnBlcnNvbmFsRGF0YXtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgcGFkZGluZzogMTZweDtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cblxuLmltYWdlLWNlbnRlcntcbiAgbWFyZ2luOjAgYXV0bztcbiAgd2lkdGg6IDcwcHg7XG4gIGhlaWdodDogNzBweDtcbiAgYm9yZGVyIDogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cblxuLnBlcmZpbEltZ3tcbiAgaDJ7XG4gICAgZm9udC1zaXplOiAxLjByZW07XG4gICAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogMXB4O1xuICB9XG4gIHB7XG4gICAgbGluZS1oZWlnaHQ6IDBweDtcbiAgICBmb250LXNpemU6IDAuOHJlbTtcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuXG4gIC5kYXRhUGVyZmlse1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbWFyZ2luLXRvcDogOHB4O1xuICB9XG59XG5cbmlvbi1hdmF0YXJ7XG4gIGltZ3tcbiAgICBib3JkZXI6IDRweCBzb2xpZCB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KVxuICB9XG59XG5cbi5jYW1lcmF7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4ubWVzc2FnZSB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDRweDtcbiAgd2hpdGUtc3BhY2U6IHByZS13cmFwO1xufVxuXG4ubXktbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeSk7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4ub3RoZXItbWVzc2FnZSB7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLnRpbWUge1xuICBjb2xvcjogI2RmZGZkZjtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IHNtYWxsO1xufVxuXG4ubWVzc2FnZS1pbnB1dCB7XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgcmVzaXplOiBub25lO1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG59XG5cbi5tc2ctYnRuIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwLjVlbTtcbiAgLS1wYWRkaW5nLWVuZDogMC41ZW07XG59XG4iXX0= */"

/***/ })

}]);
//# sourceMappingURL=perfil-perfil-module.js.map